#AndroidChatClient

安卓聊天客户端作业的源代码。

#**授权协议**
该程序使用 **GPLv3授权协议** 

#**包的划分** 
主要是分为三个包：
```
top.chenqs.mychat.client.platform  //平台相关的包
top.chenqs.mychat.client.portable  //平台无关的包
top.chenqs.mychat.common           //平台无关，且客户端和服务器均可使用的包。
```
（注：此处的平台是指Android平台或Java平台）
主要的包中的package-info.java文件有相关的文档说明。

#**阅读提示**
主要的代码是：
```
//这个类存放了整个客户端的主要实现部分。
top.chenqs.mychat.client.portable.useragent.UserAgentImpl 
```
主要看其中的run方法。这个类与界面无关。主要是里面的run方法。run方法的执行流程如下图：
![输入图片说明](http://git.oschina.net/uploads/images/2016/0709/234500_94e5ea33_3721.png "run方法的执行流程")

##如果需要运行在控制台，则：

```
//继承并扩展这个类，并重写showMessage(int type, String content)方法。
top.chenqs.mychat.client.portable.ConsoleMessageDisplayer
```

##适配其他平台或者更换界面，则：
```
//实现这个接口
top.chenqs.mychat.client.portable.MessageDisplayer
```
然后在创建实现了UserAgent的UserAgentImpl类的对象时，将具体的displayer作为参数传入构造器。

##简单的例子
可以参考以下路径的单元测试代码：
```
AndroidChatClient/source/app/src/test/java/top/chenqs/mychat/client/portable/useragent/ChatUserAgentImplTest.java
```
参考里面的testLogin()或者testRegister()待测方法。

##相关类和概念的解释
**UserAgent**：

UserAgent是一个继承了Runnable的接口。
代表使用者同服务端交互的客户端，但是不包括界面的显示。
由于界面的显示相关代码不是跨平台的，因此非跨平台的代码放在了这个包中：
```
top.chenqs.mychat.client.platform.android
```

这里给出的Android平台客户端的具体实现中，将UserAgent对象（简称UA）托管在后台服务UserAgentService.java中，因为Android平台不能直接在主线程中执行耗时操作，尤其是访问网络时使用了阻塞式的套接字。


**UserAgentImpl**：

是UserAgent的一个具体实现。
使用UA时，需要另外新建一个线程来托管使用UA，不能直接在当前线程运行。

**前台界面**：
```
//登录界面
top.chenqs.mychat.client.platform.android.ui.LoginActivity.java

//聊天界面
top.chenqs.mychat.client.platform.android.ui.ChatActivity.java
```

**后台服务**：
```
//托管了UA的后台服务
top.chenqs.mychat.client.platform.android.service.UserAgentService
```


**广播接收器**：
```
//用于后台服务与前台界面通信的广播接收器。
top.chenqs.mychat.client.platform.android.broadcast.MessageBroadcastReceiver
```


**消息响应器**：
```
//该包中存放了所有用于前台界面的消息响应方法的接口定义，和一个消息响应器的基本实现
top.chenqs.mychat.common.reactor

//一个消息响应器的基本实现
top.chenqs.mychat.common.reactor.BaseReactor

//聊天消息收发状态的响应接口
top.chenqs.mychat.common.reactor.ChatMessageReactor

//错误与调试消息的响应接口
top.chenqs.mychat.common.reactor.CommonMessageReactor

//连接状态变更消息的响应接口
top.chenqs.mychat.common.reactor.ConnectionMessageReactor

//登录状态变更消息的响应接口
top.chenqs.mychat.common.reactor.LoginMessageReactor 

//注册状态变更消息的响应接口
top.chenqs.mychat.common.reactor.RegisterMessageReactor

//服务器配置变更消息的响应接口
top.chenqs.mychat.common.reactor.SettingMessageReactor

//消息的规格
top.chenqs.mychat.common.reactor.MessageSpec
```
登录界面和聊天界面中都有消息响应器的内部类，分别实现了以上的的接口。

**消息显示器**：
```
//消息显示器
top.chenqs.mychat.client.portable.displayer.MessageDisplayer

//基于控制台的一个消息显示器
top.chenqs.mychat.client.portable.displayer.ConsoleMessageDisplayer 

//基于安卓平台的消息显示器的一个具体实现，用于显示广播接收器收到的消息。
top.chenqs.mychat.client.platform.android.displayer.BroadcastMessageDisplayer
```

**消息收发器**：
```
//封装了不同的消息的转发行为的消息收发器
top.chenqs.mychat.client.portable.io.MessageTransreceiver
```
构造时，需要传入消息显示器，在Android平台客户端中，
传入的是基于BroadcastMessageDisplayer的消息显示器，
由消息显示器来决定消息要如何显示，实现了Android和Java平台的可移植性。


**消息码**：
```
//前台界面与后台服务和UA进行通信的消息消息码。
top.chenqs.mychat.client.portable.enums.MessageType
```

**协议数据**：
```
//该包中所有的类都用来承载收发的协议数据单元（PDU）。
top.chenqs.mychat.common.beans

//聊天消息PDU
top.chenqs.mychat.common.beans.SendingMessage

//反馈消息PDU
top.chenqs.mychat.common.beans.FeedbackMessage

//该包中存放了用于创建或解析PDU的类，和相关字段的验证器。
top.chenqs.mychat.common.models
```

为了适应登录时不需要密码，注册时需要密码的协议要求，有以下三个类：
```
top.chenqs.mychat.common.models.User

//用于用户登录，继承了User，在构造器中删除了密码字段的数据
top.chenqs.mychat.common.models.LoginUser

//用于用户注册，继承了User，在构造器中加密了密码字段的数据
top.chenqs.mychat.common.models.RegisterUser
```

创建协议数据单元（即Bean序列化成PDU）、或解析协议数据单元（即PDU反序列化成具体的Bean对象）的工作，由这个类来负责：
```
//协议数据单元（即Bean序列化成PDU）的工厂类，
top.chenqs.mychat.common.models.PDUFactory
```

为了适应聊天界面中，需要将各类信息显示在聊天消息列表中，有以下的类：
```
//承载PDU的容器。
top.chenqs.mychat.common.models.PDUContainer
```
当PDUFactory反序列化PDU的时候，将反序列化的结果(包括其中的异常信息)，
放在PDU容器中，交由消息收发器，根据PDU容器中的内容，转发给前台界面。

#**消息传递的流程**


1. Android平台客户端中，消息收发器是基于广播的 **消息显示器** BroadcastMessageDisplayer，
2. **消息收发器** 会将具体的消息，通过广播转发出去。
3. **广播接收器** ，接收到广播的消息，然后给前台界面的 **Handler** 发送消息，通知前台界面获取消息。
4. 前台界面的Handler收到消息后，通过消息响应器 **Reactor** ，响应具体的消息。
5. 响应器通过与后台服务的 **Binder** ，获取对应的消息。
6. 后台的Binder，通过 **后台服务** 承载的UA来获取消息。



- 除了接收的聊天消息以外，其他所有的消息都通过广播来直接传递，而不需要通过后台服务来拉取信息。
- 当然，主动发出的聊天消息，则通过后台服务直接推入待发送的消息任务队列中。
- 而接收到的聊天消息，基于某些其他方面的考量，并不直接通过广播来传递，
- 仅仅是通过广播来告知前台界面有新消息，由前台界面来决定是否去拉取。
- 而消息的内容则保留在UA的聊天消息缓存队列中，等待前台界面通过后台服务来拉取。
- 这样做是为了适应按Home键将前台界面转入后台后，广播接收器收不到新的聊天消息，而导致聊天消息的丢失问题。


