package top.chenqs.mychat.common.beans;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.alibaba.fastjson.JSON;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chenqs on 2016/6/24.
 */
public class JsonifyUnitTest {
        @Test
        public void testUserToJson() throws Exception {
                UserName user = new UserName();
                user.setUserName("chen");

                Object actual = JSON.toJSON(user);
                Object expected = JSON.parse("{\"userName\":\"chen\"}");
                /*
                {
                        "userName":"chen"
                }
                */
                System.out.println(actual.toString());
                assertEquals(expected, actual);
        }

        @Test
        public void testSenderToJson() throws Exception {
                Sender sender = new Sender("z");
                sender.setUserPWD("1");
                sender.setUserID(10010);
                sender.setUserToken("alwaysRun");

                Object actual = JSON.toJSON(sender);
                Object expected = JSON.parse(
                    "{" +
                        "\"userName\":\"z\"," +
                        "\"userPWD\":\"1\"," +
                        "\"userID\":10010," +
                        "\"userToken\":\"alwaysRun\"" +
                        "}");
                /*
                {
                        "userName": "z",
                        "userPWD": "1",
                        "userID": 10010,
                        "userToken": "alwaysRun"
                }
                */

                System.out.println(actual.toString());
                assertEquals(expected, actual);

                sender.setUserPWD(null);
                Object actual2 = JSON.toJSON(sender);
                Object expected2 = JSON.parse(
                    "{" +
                        "\"userName\":\"z\"," +
                        "\"userID\":10010," +
                        "\"userToken\":\"alwaysRun\"" +
                        "}");
                assertEquals(expected2.toString(), actual2.toString());
        }

        @Test
        public void testProtoBaseUnitToJson() throws Exception {
                BasePDU p = new BasePDU(1073741991, 2);
                p.setMsgLength(1073741991);
                p.setMsgType(2);
                p.setSenderTimer(1466038657122130200L);

                Object actual = JSON.toJSON(p);
                Object expected = JSON.parse(
                    "{" +
                        "\"protoSign\":142857," +
                        "\"msgLength\":1073741991," +
                        "\"msgType\":2," +
                        "\"senderTimer\": 1466038657122130200" +
                        "}");
                /*
                {
                        "protoSign":　142857,
                        "msgLength":　1073741991,
                        "msgType":　2,
                        "senderTimer":　1466038657122130200,
                }

                */

                System.out.println(actual.toString());
                assertEquals(expected, actual);
        }

        @Test
        public void testActionStatusToJson() throws Exception {
                ActionStatus actionStatus = new ActionStatus();
                actionStatus.setActionMsgType(22);
                actionStatus.setActionRslMsg("success");
                actionStatus.setActionRslCode(0);
                Object actual = JSON.toJSON(actionStatus);
                Object expected = JSON.parse(
                    "{" +
                        "\"actionMsgType\": 22," +
                        "\"actionRslMsg\": \"success\"," +
                        "\"actionRslCode\": 0" +
                        "}");
                /*
                {
                        "actionMsgType": 22,
                        "actionRslMsg": "success",
                        "actionRslCode": 0
                }
                */

                System.out.println(actual.toString());
                assertEquals(expected, actual);
        }

        @Test
        public void testFeedbackMessageToJson() throws Exception {
                FeedbackMessage msg = new FeedbackMessage(new ActionStatus(22, "success", 0));
                msg.setMsgLength(1073741991);
                msg.setMsgType(2);
                msg.setSenderTimer(1466038657122130200L);

                Object actual = JSON.toJSON(msg);
                String json = "{" +
                    "\"protoSign\": 142857," +
                    "\"msgLength\": 1073741991," +
                    "\"msgType\": 2," +
                    "\"senderTimer\": 1466038657122130200," +
                    "\"actionStatus\": {" +
                    "   \"actionMsgType\": 22," +
                    "   \"actionRslMsg\": \"success\"," +
                    "   \"actionRslCode\": 0" +
                    "}" +
                    "}";
                Object expected = JSON.parse(json);
                /*
                {
                    "protoSign": 142857,
                    "msgLength": 1073741991,
                    "msgType": 2,
                    "senderTimer": 1466038657122130200,

                    "actionStatus": {
                        "actionMsgType": 22,
                        "actionRslMsg": "success",
                        "actionRslCode": 0
                    }
                }
                */
                System.out.println(actual.toString());
                assertEquals(expected, actual);
                FeedbackMessage expected_msg = JSON.parseObject(json, FeedbackMessage.class);
                assertEquals(expected_msg, msg);

        }

        @Test
        public void testChatMessageToJson() throws Exception {

                SendingMessage msg = new SendingMessage(new Sender("z", "1", 10010, "alwaysRun"), null);

                msg.setMsgLength(1073742063);
                msg.setMsgType(22);
                msg.setSenderTimer(1466038626809209300L);
                msg.setFeedbackType("s");
                msg.setPayLoad("SGVsbG8=");
                msg.addUser(new UserName("a"));

                Object actual = JSON.toJSON(msg);
                String json = "{" +
                    "\"protoSign\": 142857," +
                    "\"msgLength\": 1073742063," +
                    "\"msgType\": 22," +
                    "\"senderTimer\": 1466038626809209300," +
                    "\"feedbackType\": \"s\"," +
                    "\"sender\": {" +
                    "    \"userName\": \"z\"," +
                    "    \"userPWD\": \"1\"," +
                    "    \"userID\": 10010," +
                    "    \"userToken\": \"alwaysRun\"" +
                    "}," +
                    "\"payLoad\": \"SGVsbG8=\"," +
                    "\"userList\": [" +
                    " {\"userName\": \"a\"}" +
                    "]}";
                Object expected = JSON.parse(json);
                /*
                {
                    "protoSign": 142857,
                    "msgLength": 1073742063,
                    "msgType": 22,
                        "senderTimer": 1466038626809209300,

                    "feedbackType": "s",

                    "sender": {
                        "userName": "z",
                        "userPWD": "1",
                        "userID": 10010,
                        "userToken": "alwaysRun"
                    },
                    "payLoad": "SGVsbG8=",
                    "userList": [
                        {
                            "userName": "a"
                        }
                    ]
                }
                */

                System.out.println(actual.toString());
                assertEquals(expected, actual);

                SendingMessage expected_msg = JSON.parseObject(json, SendingMessage.class);
                assertEquals(expected_msg, msg);
        }

        @Test
        public void testDate() {
                MyDate date = new MyDate();
                long nano = System.currentTimeMillis();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
                //SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                System.out.println(sdf.format(new Date(nano)));
                //System.out.println(sdf2.format(date));

        }

        class MyDate {
                Date date;

                public MyDate() {
                        date = new Date();
                }

                public Date getDate() {
                        return date;
                }

                public void setDate(Date date) {
                        this.date = date;
                }
        }
}


