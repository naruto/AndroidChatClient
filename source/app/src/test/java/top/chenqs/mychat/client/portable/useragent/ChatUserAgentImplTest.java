package top.chenqs.mychat.client.portable.useragent;

import org.junit.Test;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

import top.chenqs.mychat.client.portable.displayer.ConsoleMessageDisplayer;
import top.chenqs.mychat.common.beans.UserName;
import top.chenqs.mychat.common.models.ChatMessage;

/**
 * Created by chenqs on 2016/6/29.
 */
public class ChatUserAgentImplTest {
        @Test
        public void testIPV4 () {
                InetSocketAddress addr;
                try {
                        addr = new InetSocketAddress("chenqs.top", 6666);
                        System.out.println(addr.toString());
                        InetAddress ipv4 = addr.getAddress();
                        assert ipv4 != null :"无法获取IP地址";
                        String ipv4str = ipv4.getHostAddress();
                        System.out.println(ipv4str);
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }

        @Test
        public void testChatUserAgent() {
                ChatUserAgent ua = new ChatUserAgentImpl(new ConsoleMessageDisplayer());
                ua.useDefaultServer();
                try {
                        if (ua.login("chen", "aabbcc") == -1) {
                                ua.displayMessage(-1, "登录失败");
                        }
                } catch (Exception e) {
                        System.out.println(e.getMessage());
                }
        }

        @Test
        public void testExtractReceivers() {
                testExtractReceivers("@张三 @李四 @王五 这里是消息正文");
                testExtractReceivers("@张三 @李四 @王五 这里是 消息正文");
                testExtractReceivers("@张三  @李四  @王五 这里是 消息正文 ");
                testExtractReceivers("@张三 @李四 @王五  这里是消息正文");
                testExtractReceivers("@张三 @李四 @王 五 这里是消息正文");
                testExtractReceivers("张三 李四 王五 这里是消息正文");
                testExtractReceivers("这里也是消息正文@张三 hello @李四 @王五 这里是消息正文");
        }

        @Test
        public void testNewInetSocketAddress() {
                String host = ChatUserAgentImpl.DEFAULT_HOST;
                int port = ChatUserAgentImpl.DEFAULT_PORT;
                SocketAddress address = new InetSocketAddress(host, port);
        }

        private void testExtractReceivers(String content) {
                System.out.println("消息原文：" + content);
                List<UserName> user_list = new ArrayList<>();
                ChatMessage.extractReceivers(content, user_list);
                System.out.println("接收者为：");
                for (UserName user : user_list) {
                        System.out.println(user.getUserName());
                }
        }

        @Test
        public void testLogin() {
                Socket client = new Socket();
                ChatUserAgent ua = new ChatUserAgentImpl(new ConsoleMessageDisplayer());
                try {

                        //client.connect(new InetSocketAddress("qnear.com", 6666), 2500);
                        Thread t = new Thread(ua);
                        t.start();
                        ua.setServer("qnear.com", 6666);
                        ua.connectServer();
                        ua.login("chenqs222", "pwd");
                        while(!ua.isAliveServer()) {/*等待连接上*/}
                        System.out.println("已连接");
                        while (!ua.isOnlineUser()){/*等待连接上*/}
                        ua.disconnectServer();

                } catch (Exception e) {
                        System.out.println(e.getMessage());
                }
        }

        @Test
        public void testRegister() {
                Socket client = new Socket();
                ChatUserAgent ua = new ChatUserAgentImpl(new ConsoleMessageDisplayer());
                try {

                        //client.connect(new InetSocketAddress("qnear.com", 6666), 2500);
                        Thread t = new Thread(ua);
                        t.start();
                        ua.setServer("qnear.com", 6666);
                        ua.connectServer();
                        ua.register("chenqs222", "pwd");
                        while(!ua.isAliveServer()) {/*等待连接上*/}
                        System.out.println("已连接");
                        while (!ua.isOnlineUser()){/*等待连接上*/}
                        ua.disconnectServer();

                } catch (Exception e) {
                        System.out.println(e.getMessage());
                }
        }


}
