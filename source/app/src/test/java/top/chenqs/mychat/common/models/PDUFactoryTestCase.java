package top.chenqs.mychat.common.models;

import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

import top.chenqs.mychat.common.beans.FeedbackMessage;
import top.chenqs.mychat.common.beans.UserName;

/**
 * Created by chenqs on 2016/7/2.
 */
public class PDUFactoryTestCase {
        @Test
        public void testCreateRegisterPDU() {
                String pdu = PDUFactory.createRegisterPDU(new User("chenqs", "helloworld"));
                System.out.println(pdu);
        }

        @Test
        public void testCreateLoginPDU(){
                String pdu = PDUFactory.createLoginPDU(new User("chenqs", "helloworld"));
                System.out.println(pdu);
        }

        @Test
        public void testCreateChatPDU() {
                List<UserName> list = new ArrayList<>();
                list.add(new UserName("user1"));
                String pdu = PDUFactory.createChatPDU(new User("chenqs", "pwd2"), list, "这里是聊天消息原文");
                System.out.println(pdu);
        }

        @Test
        public void testParseFeedbackPDU() {
                String pdu =
                    "{\n" +
                        "  \"protoSign\": 142857, \n" +
                        "  \"msgLength\": 1073741991, \n" +
                        "  \"msgType\": 2, \n" +
                        "  \"senderTimer\": 1466038657122130200, \n" +
                        "  \"actionStatus\": {\n" +
                        "    \"actionMsgType\": 22, \n" +
                        "    \"actionRslMsg\": \"success\", \n" +
                        "    \"actionRslCode\": 0\n" +
                        "  }\n" +
                        "}";
		try {

			FeedbackMessage msg = PDUFactory.parsePDU(pdu, FeedbackMessage.class);
			System.out.println(msg);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        }

	@Test
	public void testParsePDU() {
		FeedbackMessage msg = null;
		try {
			msg = PDUFactory.parsePDU("", FeedbackMessage.class);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		Assert.assertNull(msg);
		String pdu =
		    "{\n" +
			"  \"protoSign\": 142857, \n" +
			"  \"msgLength\": 1073741991, \n" +
			"  \"msgType\": 2, \n" +
			"  \"senderTimer\": 1466038657122130200, \n" +
			"  \"actionStatus\": {\n" +
			"    \"actionMsgType\": 22, \n" +
			"    \"actionRslMsg\": \"success\", \n" +
			"    \"actionRslCode\": 0\n" +
			"  }\n" +
			"}\r\n";
		try {
			msg = PDUFactory.parsePDU(pdu, FeedbackMessage.class);
			Assert.assertNotNull(msg);
			String s = JSON.toJSONString(msg);
			System.out.println(s);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

        @Test
        public void testLineSep() {
                String sep = PDUFactory.linesep;
                Assert.assertEquals(sep, "\r\n");
        }

	@Test
	public void testEscape() {
		String pdu = PDUFactory.createLoginPDU(new User("chen\\qs", "helloworld"));
		System.out.println(pdu);
		Assert.assertEquals(pdu.charAt(pdu.length()-1), '\n');
		Assert.assertEquals(pdu.charAt(pdu.length()-2), '\r');
		System.out.println(PDUFactory.unescape(pdu));
		System.out.println(PDUFactory.escape("\\"));
		System.out.println(PDUFactory.escape("\\" + PDUFactory.linesep));

	}

	@Test
	public void testUnescape() {
//		String pdu = PDUFactory.createLoginPDU(new User("chen\\qs", "helloworld"));
//		System.out.println(pdu);
//		Assert.assertEquals(pdu.charAt(pdu.length()-1), '\n');
//		Assert.assertEquals(pdu.charAt(pdu.length()-2), '\r');
//		System.out.println(PDUFactory.unescape(pdu));
		String s1 = "\\n\\r" + PDUFactory.linesep;
		s1 = PDUFactory.escape(s1);
		Assert.assertEquals(" \\\\ n \\\\ r \\n ", s1);
		s1 = PDUFactory.unescape(s1);
		Assert.assertEquals("\\n\\r"+PDUFactory.linesep, s1);
		String s2 = "\\n";
		s2 = PDUFactory.unescape(s2);
		Assert.assertEquals("\\n", s2);


	}
}
