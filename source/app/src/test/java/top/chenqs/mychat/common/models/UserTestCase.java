package top.chenqs.mychat.common.models;

import org.junit.Test;

/**
 * Created by chenqs on 2016/7/2.
 */
public class UserTestCase {
        @Test
        public void testNewUser() {
                User u = new User("chenqs", "helloworld");
                u.getSender().setUserToken("alwaysRun");
                String result = u.toString();
                System.out.println(result);
        }

        @Test public void testLoginUser() {
                User u = new LoginUser("chenqs", "pwd");
                String s = PDUFactory.createLoginPDU(u);
                System.out.println(s);
        }

        @Test public void testRegisterUser() {
                User u = new RegisterUser("chenqs", "pwd");
                String s = PDUFactory.createRegisterPDU(u);
                System.out.println(s);
        }

}
