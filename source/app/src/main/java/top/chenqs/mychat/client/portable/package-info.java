/**
 * 该包下的所有子包均为跨平台的包。
 * 包内的所有代码要求必须是Android平台和Java平台通用的。
 * @see top.chenqs.mychat.client.portable.displayer
 * @see top.chenqs.mychat.client.portable.useragent
 * @version 1.0
 */
package top.chenqs.mychat.client.portable;