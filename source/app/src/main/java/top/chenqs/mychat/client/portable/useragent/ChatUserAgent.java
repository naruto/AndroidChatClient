package top.chenqs.mychat.client.portable.useragent;

import top.chenqs.mychat.common.models.ChatMessage;

/**
 * Created by chenqs on 2016/6/26.
 * <p/>
 * 需求描述
 * 1）注册用户：可输入服务器地址，端口，注册用户名，口令，多个消息目标用户
 * 2）向一个目标用户发文本消息；
 * 3）向几个目标用户发文本消息；
 * 4）确认目标用户收到消息；
 * 5）接收文本消息；
 * 6）显示接收的文本消息，在软件界面显示接收到的任何消息；
 * 7）显示软件运行过程中的错误消息，如连接服务失败等。
 */
public interface ChatUserAgent extends Runnable {
	//登录
	int login(String username, String password) throws Exception;

	//注册
	int register(String username, String password) throws Exception;

	//注销
	int logout() throws Exception;

	void useDefaultServer();

	/**
	 * 设置服务器
	 * @param host 主机名或IP地址，若传入null或""，则表示使用默认值
	 * @param port 端口号，若传入-1，则表示使用默认值
	 */
	void setServer(String host, int port);

	void connectServer();

	/**
	 * 发送聊天消息
	 *
	 * @param content
	 * @throws Exception
	 */
	void sendMessage(String content) throws Exception;

	// 每个ChatUserAgent中都保存了一个聊天消息队列，
	// 该方法用于检测聊天消息队列中是否还有未获取的消息
	boolean hasNextMessage();

	ChatMessage fetchMessage();

	void disconnectServer();

	/**
	 * 线程是否已运行
	 * @return
	 */
	boolean isRunning();

	void close();

	void displayMessage(int what, String content);

	/**
	 * 判断服务器配置是否有效
	 * @return
	 */
	boolean isValidConfig();

	/**
	 * 判断是否已连接服务器
	 */
	boolean isAliveServer();

	/**
	 * 判断用户是否已登陆
	 *
	 * @return
	 */
	boolean isOnlineUser();
}
