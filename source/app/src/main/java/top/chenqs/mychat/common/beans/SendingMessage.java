package top.chenqs.mychat.common.beans;

import com.alibaba.fastjson.annotation.JSONType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 聊天消息、注册消息的协议数据单元
 * <p>Created by chenqs on 2016/6/24.
 */
@JSONType(orders={"protoSign", "msgLength", "msgType", "feedbackType",
    "senderTimer", "sender", "payLoad", "userList"})
public class SendingMessage extends BasePDU {

        /**
         * 消息反馈类型，"s"：表示服务端反馈，即服务端收到消息后要通知客户端自己收到了该消息
         */
        protected String feedbackType;

        /**
         * 发送者信息
         */
        protected Sender sender;

        /**
         * 要发送给目标用户的信息，以UTF8编码
         */
        protected String payLoad;     //: "SGVsbG8="　

        /**
         * 接收消息的目标用户列表，必选
         */
        protected List<UserName>  userList = new ArrayList<UserName>();
        /*":　        [{　
        　　　　　　　　　　　　"userName":　"a"　//目标用户名a
        　　　　　　　　},{　
        　　　　　　　　　　　　"userName":　"张三"　//目标用户名张三
        　　　　　　　　},{　
        　　　　　　　　　　　　"userName":　"李四"　//目标用户名李四
        　　　　　　　　}]
        */

        public SendingMessage(Sender sender, List<UserName> userList) {
                this(sender, "", "s", userList);
        }

        public SendingMessage(Sender sender, String payLoad,
                              String feedbackType, List<UserName> userList) {
                super(payLoad.length(), 22);
                this.sender = sender;
                this.payLoad = payLoad;
                this.feedbackType = feedbackType;
                this.userList = userList;
        }

        public String getFeedbackType() {
                return feedbackType;
        }

        public void setFeedbackType(String feedbackType) {
                this.feedbackType = feedbackType;
        }

        public Sender getSender() {
                return sender;
        }

        public void setSender(Sender sender) {
                this.sender = sender;
        }

        public String getPayLoad() {
                return payLoad;
        }

        public void setPayLoad(String payLoad) {
                this.payLoad = payLoad;
        }

        public List<UserName> getUserList() {
                return userList;
        }

        public void setUserList(List<UserName> userList) {
                this.userList = userList;
        }

        public void addUser(UserName user) {
                if (userList == null)
                        userList = new ArrayList<>();
                userList.add(user);

        }

        @Override
        public String toString() {
                return "SendingMessage{" +
                    super.toString() + '\'' +
                    ", feedbackType='" + feedbackType + '\'' +
                    ", sender=" + sender +
                    ", payLoad='" + payLoad + '\'' +
                    ", userList=" + userList +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                SendingMessage that = (SendingMessage) o;

                if (!feedbackType.equals(that.feedbackType)) return false;
                if (!sender.equals(that.sender)) return false;
                if (!payLoad.equals(that.payLoad)) return false;
                return userList.equals(that.userList);

        }

        @Override
        public int hashCode() {
                int result = feedbackType.hashCode();
                result = 31 * result + sender.hashCode();
                result = 31 * result + payLoad.hashCode();
                result = 31 * result + userList.hashCode();
                return result;
        }
}
