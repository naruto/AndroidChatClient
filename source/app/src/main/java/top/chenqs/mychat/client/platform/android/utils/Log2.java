package top.chenqs.mychat.client.platform.android.utils;

import android.util.Log;

/**
 * Created by chenqs on 2016/7/2.
 */
public class Log2 {
	static private boolean is_enable = true;

	static public void closeLog() {
		is_enable = false;
	}

	static public void d(String tag, String msg) {
		if (is_enable) {
			Log.d(tag, msg);
		}
	}

	/**
	 * 当obj==null时，打印msg，如果msg==null，则打印默认消息。
	 * 当obj!=null时，不打印
	 *
	 * @param obj 待测试的对象
	 * @param msg 待打印的日志
	 */
	static public void log_if_null(Object obj, String msg) {
		if (obj != null) return;

		String _msg = msg == null ? obj.toString() + "==null" : msg;
		d("Chat", _msg);
	}

	static public void d(String msg) {
		if (msg != null) {
			d("Chat", msg);
		}
	}
}
