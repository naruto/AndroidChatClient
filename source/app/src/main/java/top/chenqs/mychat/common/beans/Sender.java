package top.chenqs.mychat.common.beans;

import com.alibaba.fastjson.annotation.JSONType;

/**
 * 协议数据单元的一部分——发送者。
 *
 * <p>如果包含userName,userPWD,当服务端不存在该用户信息时，服务端自动注册该用户
 * <p>Created by chenqs on 2016/6/24.
 */
@JSONType(orders = {"userName", "userPWD", "userID" , "userToken"})
public class Sender {
        //用户名，必选，必须包含，缺失则任务失败
        protected String userName;    //":　"z"

        //口令，可选，如注册用户，则必须包含
        protected String userPWD;     //":　"1"

        //用户帐号，可选
        protected int userID;         //":　10010

        //用户令牌，可选
        protected String userToken;   //":　"alwaysRun"　

        public Sender(String userName) {
                this(userName, null, 0, null);
        }

        public Sender(String userName, String userPWD) {
                this(userName, userPWD, 10010, null);
        }

        public Sender(String userName, String userPWD, int userID) {
                this(userName, userPWD, userID, null);
        }

        public Sender(String userName, String userPWD, int userID, String userToken) {
                this.userName = userName;
                this.userPWD = userPWD;
                this.userID = userID;
                this.userToken = userToken;
        }

        public String getUserName() {
                return userName;
        }

        public void setUserName(String userName) {
                this.userName = userName;
        }

        public String getUserPWD() {
                return userPWD;
        }

        public void setUserPWD(String userPWD) {
                this.userPWD = userPWD;
        }

        public int getUserID() {
                return userID;
        }

        public void setUserID(int userID) {
                this.userID = userID;
        }

        public String getUserToken() {
                return userToken;
        }

        public void setUserToken(String userToken) {
                this.userToken = userToken;
        }

        @Override
        public String toString() {
                return "Sender{" +
                    "userName='" + userName + '\'' +
                    ", userPWD='" + userPWD + '\'' +
                    ", userID=" + userID +
                    ", userToken='" + userToken + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                Sender sender = (Sender) o;

                if (userID != sender.userID) return false;
                if (!userName.equals(sender.userName)) return false;
                if (!userPWD.equals(sender.userPWD)) return false;
                return userToken.equals(sender.userToken);

        }

        @Override
        public int hashCode() {
                int result = userName.hashCode();
                result = 31 * result + userPWD.hashCode();
                result = 31 * result + userID;
                result = 31 * result + userToken.hashCode();
                return result;
        }
}
