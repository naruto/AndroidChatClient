package top.chenqs.mychat.common.beans;

/**
 * Created by chenqs on 2016/6/24.
 */
public class UserName {
        protected String userName;

        public UserName() {
        }

        public UserName(String userName) {
                this.userName = userName;
        }

        public String getUserName() {
                return userName;
        }

        public void setUserName(String userName) {
                this.userName = userName;
        }

        @Override
        public String toString() {
                return "UserName{" +
                    "userName='" + userName + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                UserName user = (UserName) o;

                return userName.equals(user.userName);

        }

        @Override
        public int hashCode() {
                return userName.hashCode();
        }
}
