package top.chenqs.mychat.common.reactor;

/**
 * Created by chenqs on 2016/7/3.
 */
public interface LoginMessageReactor {
	void onLoginOk();
	void onLoginTrying();	//正在请求登录
	void onLoginFail(int what, String reason);
}
