package top.chenqs.mychat.common.models;

import top.chenqs.mychat.common.beans.FeedbackMessage;
import top.chenqs.mychat.common.beans.SendingMessage;

 /**
 * 协议数据单元容器。
  * 承载不同类型的PDU和异常数据。
 */
public class PDUContainer {
	private FeedbackMessage feedback;
	private SendingMessage sending;
	private Exception exception;

	public FeedbackMessage getFeedback() {
		return feedback;
	}

	public void setFeedback(FeedbackMessage feedback) {
		this.feedback = feedback;
	}

	public SendingMessage getSending() {
		return sending;
	}

	public void setSending(SendingMessage sending) {
		this.sending = sending;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public boolean hasFeedback(){
		return feedback != null;
	}
	public boolean hasSending(){
		return sending != null;
	}
	public boolean hasException(){
		return exception != null;
	}
}