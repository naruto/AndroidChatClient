package top.chenqs.mychat.client.platform.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import top.chenqs.mychat.client.platform.android.broadcast.MessageBroadcastReceiver;
import top.chenqs.mychat.client.platform.android.displayer.BroadcastMessageDisplayer;
import top.chenqs.mychat.client.platform.android.utils.Log2;
import top.chenqs.mychat.client.portable.displayer.MessageDisplayer;
import top.chenqs.mychat.client.portable.enums.MessageType;
import top.chenqs.mychat.client.portable.useragent.ChatUserAgent;
import top.chenqs.mychat.client.portable.useragent.ChatUserAgentImpl;
import top.chenqs.mychat.common.models.ChatMessage;

public class UserAgentService extends Service {
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
	private UserAgentBinder binder;
	private ChatUserAgent ua;

	public UserAgentService() {
	}

	@Override
	public void onCreate() {
		super.onCreate();
		MessageDisplayer displayer = new BroadcastMessageDisplayer(
		    this, MessageBroadcastReceiver.ACTION);

		ua = new ChatUserAgentImpl(displayer);
		binder = new UserAgentBinder();
		Log2.d("Chat", "服务已创建");
		if (ua != null && !ua.isRunning())
			new Thread(ua).start();

	}

	@Override
	public void onDestroy() {
		Toast.makeText(this, "服务已销毁", Toast.LENGTH_LONG).show();
		super.onDestroy();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Toast.makeText(this, "服务已解绑", Toast.LENGTH_LONG).show();
		return super.onUnbind(intent);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Toast.makeText(this, "服务已启动", Toast.LENGTH_LONG).show();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onRebind(Intent intent) {
		Toast.makeText(this, "服务已重新绑定", Toast.LENGTH_LONG).show();
		super.onRebind(intent);
	}

	@Override
	public IBinder onBind(Intent intent) {
		Toast.makeText(this, "服务已绑定", Toast.LENGTH_LONG).show();
		return binder;
	}

	/**
	 * 发送登录状态广播
	 * @param type {@link top.chenqs.mychat.client.portable.enums.MessageType}
	 * @param reason 错误原因
	 */
	protected void sendLoginBroadcast(int type, String reason) {
		final String action = "top.chenqs.mychat.client.platform.android.intent.action.MESSAGE";
		Intent broadcast_intent = new Intent(action);
		broadcast_intent.putExtra("what", type);
		if (reason != null) {
			broadcast_intent.putExtra("reason", reason);
		}
		sendBroadcast(broadcast_intent);
	}

	private String fetchNewMessage() {
		ChatMessage msg = ua.fetchMessage();
		if (msg == null)
			return null;
		String date = sdf.format(msg.getTime());
		StringBuilder builder = new StringBuilder();
		builder
		    .append(msg.getSender())
		    .append(": ")
		    .append(msg.getContent())
		    .append("\t")
		    .append(date);
		return builder.toString();
	}

	/**
	 * 提供给UI操纵后台服务的Binder
	 */
	public class UserAgentBinder extends Binder {

		public UserAgentBinder() {

		}

		/**
		 * 发送聊天消息
		 * @param content 聊天消息原文
		 */
		public void sendChatMessage(String content) {
			try {
				ua.sendMessage(content);
			} catch (Exception e) {

			}
		}

		/**
		 * 登录
		 * @param username 用户名
		 * @param password 密码
		 */
		public void login(String username, String password) {
			try {
				ua.login(username, password);
			} catch (Exception e) {
				sendLoginBroadcast(MessageType.LOGIN_FAIL, "未知原因");
				sendLoginBroadcast(MessageType.ERROR, e.getMessage());
			}
		}

		/**
		 * 注册
		 * @param username 用户名
		 * @param password 密码
		 */
		public void register(String username, String password) {
			try {
				ua.register(username, password);
			} catch (Exception e) {
				sendLoginBroadcast(MessageType.REGISTER_FAIL, e.getMessage());
			}
		}

		/**
		 * 注销
		 */
		public void logout() {
			try {
				ua.logout();
			} catch (Exception e) {

			}
		}



		public String fetchNewMessage() {
			return fetchNewMessage();
		}

		public boolean hasNextNewMessage() {
			return ua.hasNextMessage();
		}

		public void setServer(String host, int port) {
			ua.setServer(host, port);
		}

		public void setServer() {
			ua.useDefaultServer();
		}



		public void connectServer() {
			ua.connectServer();
		}

		/**
		 * 判断配置是否有效
		 * @return
		 */
		public boolean isValidConfig() {
			return ua.isValidConfig();
		}

		/**
		 * 判断服务器是否已连接
		 */
		public boolean isServerAlive() {
			return ua.isAliveServer();
		}

		/**
		 * 判断用户是否已登录
		 */
		public boolean isUserOnline() {
			return ua.isOnlineUser();
		}
	}
}
