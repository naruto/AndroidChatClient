package top.chenqs.mychat.common.reactor;

/**
 * Created by chenqs on 2016/7/3.
 */
public interface RegisterMessageReactor {
	void onRegisterOk();
	void onRegisterFail(String reason);
}
