package top.chenqs.mychat.client.platform.android.ui;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import top.chenqs.mychat.R;
import top.chenqs.mychat.client.platform.android.broadcast.MessageBroadcastReceiver;
import top.chenqs.mychat.client.portable.enums.MessageType;
import top.chenqs.mychat.client.platform.android.service.UserAgentService;
import top.chenqs.mychat.client.platform.android.utils.Log2;
import top.chenqs.mychat.common.models.ChatMessage;
import top.chenqs.mychat.common.reactor.BaseReactor;
import top.chenqs.mychat.common.reactor.ChatMessageReactor;
import top.chenqs.mychat.common.reactor.CommonMessageReactor;
import top.chenqs.mychat.common.reactor.ConnectionMessageReactor;
import top.chenqs.mychat.common.reactor.MessageSpec;

public class ChatActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {
	private Intent ua_service_intent;
	private DrawerLayout drawer;
	private NavigationView navigationView;
	private Toolbar toolbar;
	private ListView lv__message_list;
	private EditText et__message_text;
	private Button btn__send_message;
	private Toast toast;

	final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");


	//消息列表初始化
	private List<String> message_list = new ArrayList<>();

	ArrayAdapter<String> adapter;
	MessageBroadcastReceiver receiver;

	//Binder及ServiceConnection 初始化
	UserAgentService.UserAgentBinder ua_binder;
	TextView tv__msg;
	ServiceConnection connection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			ua_binder = (UserAgentService.UserAgentBinder) service;
			Log2.d("Chat", "ChatActivity已连接UA服务");
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log2.d("Chat", "ChatActivity已断开UA服务连接");

		}
	};

	Reactor reactor;

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message message) {
			Bundle detail = message.getData();
			String reason = detail.getString("reason");
			int what = message.what;
			MessageSpec spec = new MessageSpec(what, reason);
			if (reactor.react(spec)) {
				return;
			}
			Log2.d("Chat", "ChatActivity handleMessage未完全响应所有消息");
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log2.d("Chat", "聊天界面 onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);

		toast = Toast.makeText(this, null, Toast.LENGTH_LONG);

		ua_service_intent = new Intent(ChatActivity.this, UserAgentService.class);
		startUserAgentService();

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		//初始化左侧的抽屉
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		assert drawer != null;
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
		    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		navigationView = (NavigationView) findViewById(R.id.nav_view);
		assert navigationView != null;
		navigationView.setNavigationItemSelectedListener(this);

		//初始化列表及其适配器
		lv__message_list = (ListView) findViewById(R.id.lv__message_list);
		assert lv__message_list != null;
		adapter = new ArrayAdapter<>(this,
		    R.layout.msg_item_view,
		    R.id.item__new_message, message_list);
		lv__message_list.setAdapter(adapter);

		//初始化聊天消息输入框
		et__message_text = (EditText) findViewById(R.id.et__message_text);
		assert et__message_text != null;

		//初始化发送按钮
		btn__send_message = (Button) findViewById(R.id.btn__send_message);
		assert btn__send_message != null;
		btn__send_message.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String msg = et__message_text.getText().toString();
				if (msg != null && !msg.isEmpty()) {
					ua_binder.sendChatMessage(msg); //通过网络发送聊天消息
					showMessageInList(msg.trim());
					et__message_text.getText().clear();
				}
			}
		});

		receiver = new MessageBroadcastReceiver();
		reactor = new Reactor(this.toast);
	}

	@Override
	protected void onResume() {
		Log2.d("Chat", "聊天界面 onResume");
		IntentFilter filter = new IntentFilter();
		filter.addAction(MessageBroadcastReceiver.ACTION);
		bindUserAgentService();
		registerReceiver(this.receiver, filter);
		super.onResume();

	}

	@Override
	protected void onPause() {
		Log2.d("Chat", "聊天界面 onPause");
		unbindUserAgentService();
		unregisterReceiver(this.receiver);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		stopUserAgentService();
		super.onDestroy();
	}

	private long exitTime = 0;

	@Override
	public void onBackPressed() {
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		//1秒内连续按两次则退出程序
		else if (System.currentTimeMillis() - exitTime > 1000) {
			toast.setText("再按一次退出");
			toast.show();
			exitTime = System.currentTimeMillis();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chat, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		if (id == R.id.nav_manage) {

		} else if (id == R.id.nav_share) {

		}
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	//在列表中显示一条消息
	public void showMessageInList(String content) {
		if (content == null || content.isEmpty()) {
			return;
		}

		message_list.add(content + "\t " + sdf.format(new Date()));
		adapter.notifyDataSetChanged();
		lv__message_list.smoothScrollToPosition(message_list.size());
	}

	class Reactor extends BaseReactor implements ChatMessageReactor,
	    ConnectionMessageReactor, CommonMessageReactor {
		private Toast toast2;

		public Reactor(Toast t) {
			if (t == null)
				throw new RuntimeException("Reactor无法构造，toast参数不能为null");
			else
				toast2 = t;
		}

		private void showTips(String text) {
			Log2.d(text);
			toast2.setText(text);
			toast2.show();
		}

		@Override
		public boolean react(MessageSpec spec) {
			if (spec == null) return false;

			int what = spec.getWhat();
			String reason = spec.getReason();

			switch (what) {
			case MessageType.RECV_CHAT_MSG_OK:
				onReceiveNewChatMessage();
				break;
			case MessageType.SERVER_CONN_OK:
				onServerConnOK(reason);
				break;
			case MessageType.SERVER_CONN_ABORT:
				onServerConnAbort(reason);
				break;
			case MessageType.SERVER_CONN_TRYING:
				onServerConnTrying(reason);
				break;
			case MessageType.SEND_CHAT_MSG_OK:
				onSendChatMessageOK();
				break;
			case MessageType.SEND_CHAT_MSG_FAIL:
				onSendChatMessageFail(reason);
				break;
			case MessageType.ERROR:
				onError(reason);
				break;
			case MessageType.DEBUG:
				onDebug(reason);
				break;
			default:
				return super.react(spec);
			}
			return true;
		}

		public void onReceiveNewChatMessage() {
			while (ua_binder.hasNextNewMessage()) {
				String msg = ua_binder.fetchNewMessage();
				showMessageInList(msg);
			}
		}

		public void onSendChatMessageOK() {
			showTips("消息已发送");
		}

		public void onSendChatMessageFail(String reason) {
			showTips("消息发送失败");
			showTips("原因：" + reason);
		}

		public void onError(String reason) {
			if (reason == null) {
				showTips("onError参数reason不能为null");
			}
			showMessageInList("错误：" + reason);
		}

		public void onDebug(String reason) {
			String text = "调试信息：" + reason;
			showTips(text);
		}

		public void onServerConnOK(String reason) {
			showMessageInList("服务器连接成功");
		}

		public void onServerConnFail(String reason) {
			showMessageInList("服务器连接失败，原因：" + (reason == null ? "无" : reason));
		}

		public void onServerConnAbort(String reason) {
			showMessageInList("服务器连接中断，原因：" + (reason == null ? "无" : reason));
		}

		public void onServerConnClose(String reason) {
			showMessageInList("服务器关闭了连接，原因：" + (reason == null ? "无" : reason));
		}

		@Override
		public void onServerDisconnected(String reason) {
			String text = "连接已断开，原因：" + reason;
			showTips(text);
		}

		@Override
		public void onServerConnTrying(String reason) {
			String text = "正在连接服务器";
			showTips(text);
		}

		@Override
		public void onServerConnected(String reason) {
			String text = "已连接到服务器";
			showTips(text);
		}
	}


	private void startUserAgentService() {
		Log2.d("Chat", "聊天界面 启动 ua_service");
		startService(ua_service_intent);
	}

	private void bindUserAgentService() {
		Log2.d("Chat", "聊天界面 绑定 ua_service");
		bindService(ua_service_intent, connection, BIND_AUTO_CREATE);
	}

	private void unbindUserAgentService() {
		Log2.d("Chat", "聊天界面 解绑 ua_service");
		unbindService(connection);
	}

	private void stopUserAgentService() {
		Log2.d("Chat", "聊天界面 停止 ua_service");
		stopService(ua_service_intent);
	}


}
