package top.chenqs.mychat.common.reactor;

import top.chenqs.mychat.client.portable.enums.MessageType;

/**
 * Created by chenqs on 2016/7/3.
 */
public interface ConnectionMessageReactor {
	void onServerConnOK(String reason);
	void onServerConnTrying(String reason);
	void onServerConnFail(String reason);
	void onServerConnAbort(String reason);
	void onServerConnClose(String reason);
	void onServerDisconnected(String reason);
	//void onServerConnecting(String reason);
	void onServerConnected(String reason);
}
