package top.chenqs.mychat.common.reactor;

import android.widget.Toast;

import top.chenqs.mychat.client.platform.android.utils.Log2;
import top.chenqs.mychat.common.models.ChatMessage;

/**
 * Created by chenqs on 2016/7/3.
 */
public interface ChatMessageReactor {
	void onReceiveNewChatMessage();
	void onSendChatMessageOK() ;
	void onSendChatMessageFail(String reason);
}
