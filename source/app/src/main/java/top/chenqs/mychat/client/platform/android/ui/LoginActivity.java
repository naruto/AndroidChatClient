package top.chenqs.mychat.client.platform.android.ui;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AlertDialog;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import top.chenqs.mychat.R;
import top.chenqs.mychat.client.platform.android.broadcast.MessageBroadcastReceiver;
import top.chenqs.mychat.client.portable.enums.MessageType;
import top.chenqs.mychat.client.platform.android.service.UserAgentService;
import top.chenqs.mychat.client.platform.android.utils.Log2;
import top.chenqs.mychat.common.models.Validator;
import top.chenqs.mychat.common.reactor.BaseReactor;
import top.chenqs.mychat.common.reactor.CommonMessageReactor;
import top.chenqs.mychat.common.reactor.ConnectionMessageReactor;
import top.chenqs.mychat.common.reactor.LoginMessageReactor;
import top.chenqs.mychat.common.reactor.MessageSpec;
import top.chenqs.mychat.common.reactor.RegisterMessageReactor;
import top.chenqs.mychat.common.reactor.SettingMessageReactor;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity
    implements OnClickListener, TextView.OnEditorActionListener,ServiceConnection {

	private EditText et__username;
	private EditText et__password;
	private EditText et__server_host;
	private EditText et__server_port;

	private boolean is_doing_login = false;
	private Intent ua_service_intent;
	private int actual_port = 6666;
	private String actual_host = "qnear.com";
	private String actual_username;
	private String actual_password;

	UserAgentService.UserAgentBinder ua_binder;
	MessageBroadcastReceiver receiver;
	Checker checker = new Checker();
	IntentFilter filter = new IntentFilter(MessageBroadcastReceiver.ACTION);
	Toast toast;

	Reactor reactor;
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message message) {
			Log2.d("Chat", "LoginActivity handleMessage准备响应消息");
			Bundle detail = message.getData();
			String reason = detail.getString("reason");
			int what = message.what;
			Log2.d(what + "  " + reason);
			MessageSpec spec = new MessageSpec(what, reason);
			if (reactor.react(spec)) {
				return;
			}
			Log2.d("Chat", "LoginActivity handleMessage未完全响应所有消息");
			Log2.d(what + "  " + reason);
		}
	};


	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		ua_binder = (UserAgentService.UserAgentBinder) service;
		Log2.d("Chat", "LoginActivity已连接UA服务");
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		Log2.d("Chat", "LoginActivity已断开UA服务连接");

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log2.d("Chat", "登录界面 onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		toast = Toast.makeText(this, null, Toast.LENGTH_LONG);

		// 设置登录表单
		et__username = (EditText) findViewById(R.id.et__username);
		assert et__username != null;
		et__username.setOnEditorActionListener(this);

		et__password = (EditText) findViewById(R.id.et__password);
		assert et__password != null;
		et__password.setOnEditorActionListener(this);

		et__server_host = (EditText) findViewById(R.id.et__server_host);
		assert et__server_host != null;
		et__server_host.setOnEditorActionListener(this);

		et__server_port = (EditText) findViewById(R.id.et__server_port);
		assert et__server_port != null;
		et__server_port.setOnEditorActionListener(this);

		Button btn__login = (Button) findViewById(R.id.btn__login);
		assert btn__login != null;
		btn__login.setOnClickListener(this);

		Button btn__register = (Button) findViewById(R.id.btn__register);
		assert btn__register != null;
		btn__register.setOnClickListener(this);

		//启动聊天后台服务
		ua_service_intent = new Intent(LoginActivity.this, UserAgentService.class);
		startUserAgentService();

		//登录状态广播接收器
		receiver = new MessageBroadcastReceiver(this.handler);

		reactor = new Reactor(this.toast);

	}

	@Override
	protected void onResume() {
		Log2.d("Chat", "登录界面 onResume");
		this.is_doing_login = false;
		registerReceiver(this.receiver, filter);
		bindUserAgentService();
		if (ua_binder != null && ua_binder.isUserOnline()) {
			//如果检测到已登陆，则打开聊天界面
			startChatActivity();
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		unbindService(this);
		unregisterReceiver(receiver);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		stopUserAgentService();
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn__login:
			btn__login__onClick();
			break;
		case R.id.btn__register:
			btn__register__onClick();
			break;
		default:
		}
	}

	@Override
	public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
		switch (textView.getId()) {
		case R.id.et__username:
			return et__username__onEditorAction(textView, id, keyEvent);
		case R.id.et__password:
			return et__password__onEditorAction(textView, id, keyEvent);
		case R.id.et__server_host:
			return et__server_host__onEditorAction(textView, id, keyEvent);
		case R.id.et__server_port:
			return et__server_port__onEditorAction(textView, id, keyEvent);
		default:
			return false;
		}
	}


	private long exitTime = 0;

	@Override
	public void onBackPressed() {
		//1秒内连续按两次则退出程序
		if (System.currentTimeMillis() - exitTime > 1000) {
			Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			super.onBackPressed();
		}
	}

	//检测服务是否已运行
	private boolean UAServiceIsRunning() {
		ActivityManager manager
		    = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		final String service_name
		    = "top.chenqs.mychat.client.platform.android.service.UserAgentService";

		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (service_name.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private void startUserAgentService() {
		startService(ua_service_intent);
	}

	private void bindUserAgentService() {
		bindService(ua_service_intent, this, BIND_AUTO_CREATE);
	}

	private void startChatActivity() {
		startActivity(new Intent(LoginActivity.this, ChatActivity.class));
	}

	private void stopUserAgentService() {
		stopService(ua_service_intent);
	}

	private boolean et__username__onEditorAction(TextView view, int action_id, KeyEvent event) {
		return action_id == R.id.action__login || action_id == EditorInfo.IME_NULL;
	}

	private boolean et__password__onEditorAction(TextView view, int action_id, KeyEvent event) {
		return action_id == R.id.action__login || action_id == EditorInfo.IME_NULL;
	}

	private boolean et__server_host__onEditorAction(TextView view, int action_id, KeyEvent event) {
		//检测是否输入完成，输入完成后才能继续下一步检测
		if (action_id != EditorInfo.IME_ACTION_NEXT) {
			return false;
		}

		Log2.d("Chat", "检查");
		String hostname = et__server_host.getText().toString();
		try {
			checker.check(et__server_host, hostname, Checker.TARGET_HOSTNAME);
		} catch (Exception e) {

		}
		return true;
	}

	private boolean et__server_port__onEditorAction(TextView view, int action_id, KeyEvent event) {
		//检测是否输入完成，输入完成后才能继续下一步检测
		if (action_id != EditorInfo.IME_ACTION_DONE) {
			return false;
		}

		String port = et__server_port.getText().toString();
		try {
			checker.check(et__server_port, port, Checker.TARGET_PORT);
		} catch (Exception e) {

		}
		return true;
	}

	private void btn__login__onClick() {
		attemptLogin();
	}

	private void btn__register__onClick() {
		//TODO 弹出对话框 注册表单
		LinearLayout register_form = (LinearLayout) getLayoutInflater()
		    .inflate(R.layout.register_dialog_layout, null);
		final EditText et__new_user = (EditText) register_form.findViewById(R.id.et__new_username);
		final EditText et__new_pwd1 = (EditText) register_form.findViewById(R.id.et__new_password_1);
		final EditText et__new_pwd2 = (EditText) register_form.findViewById(R.id.et__new_password_2);

		new AlertDialog.Builder(this)
		    .setIcon(R.drawable.ic_user_register_black_24dp)
		    .setTitle(R.string.title__register_dialog)
		    .setView(register_form)
		    .setPositiveButton(R.string.action__register,        //注册
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String user = et__new_user.getText().toString();
					String pwd1 = et__new_pwd1.getText().toString();
					String pwd2 = et__new_pwd2.getText().toString();

					View focusView = null;

					//校验必须的字段
					try {
						focusView = et__new_user;
						checker.check(et__new_user, user, Checker.TARGET_USERNAME);
						focusView = et__new_pwd1;
						checker.check(et__new_pwd1, pwd1, Checker.TARGET_PASSWORD);
						focusView = et__new_pwd2;
						checker.check(et__new_pwd2, pwd2, Checker.TARGET_PASSWORD);
						checker.check(et__new_pwd1, pwd1, et__new_pwd2, pwd2);
					} catch (Exception e) {
						focusView.requestFocus();
						return;
					}

					attemptRegister(user, pwd1);
				}
			})
		    .setNegativeButton(R.string.action__cancel,          //取消
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			})
		    .create()
		    .show();
	}

	class Reactor extends BaseReactor implements CommonMessageReactor, LoginMessageReactor,
	    RegisterMessageReactor, ConnectionMessageReactor, SettingMessageReactor
	{
		private Toast toast2;
		public Reactor(Toast t) {
			if (t == null)
				throw new RuntimeException("Reactor无法构造，toast参数不能为null");
			else
				this.toast2 = t;

		}

		@Override
		public boolean react(MessageSpec spec) {
			int what = spec.getWhat();
			String reason = spec.getReason();
			switch (what) {
			case MessageType.LOGIN_OK:
				onLoginOk();
				break;
			case MessageType.LOGIN_FAIL_PWD_ERROR:
			case MessageType.LOGIN_FAIL:
				onLoginFail(what, reason);
				break;
			case MessageType.LOGIN_TRYING:
				onLoginTrying();
				break;
			case MessageType.REGISTER_OK:
				onRegisterOk();
				break;
			case MessageType.REGISTER_FAIL_EXISTENT_USER:
				onRegisterFail("用户名已被占用");
				break;
			case MessageType.REGISTER_FAIL:
				onRegisterFail(reason);
				break;
			case MessageType.SETTING_HOSTNAME_OK:
				onSettingHostOk(reason);
				break;
			case MessageType.SETTING_HOSTNAME_FAIL:
				onSettingHostError(reason);
				break;
			case MessageType.SETTING_PORT_OK:
				onSettingPortOk(reason);
				break;
			case MessageType.SETTING_PORT_FAIL:
				onSettingHostError(reason);
				break;
			case MessageType.ERROR:
				onError(reason);
				break;
			case MessageType.DEBUG:
				onDebug(reason);
				break;
			case MessageType.SERVER_CONN_FAIL:
				onServerConnFail(reason);
			case MessageType.SERVER_CONN_CLOSE:
				onServerConnClose(reason);
				break;
			case MessageType.SERVER_CONN_TRYING:
				onServerConnTrying(reason);
				break;
			case MessageType.SERVER_CONN_OK:
				onServerConnOK(reason);
				break;
			default:
				return super.react(spec);
			}
			return true;
		}

		public void onLoginOk() {
			String text = "登录成功";
			showTips(text);
			startChatActivity();
		}

		public void onLoginFail(int what, String reason) {
			is_doing_login = false;
			String msg = null;
			switch (what) {
			case MessageType.LOGIN_FAIL:
				msg = "登录失败，原因：" + reason;
				break;
			case MessageType.LOGIN_FAIL_PWD_ERROR:
				msg = "登录失败，密码错误";
				break;
			}
			if (msg != null) {
				showTips(msg);
			}
		}

		public void onLoginTrying() {
			String text = "正在登录";
			showTips(text);
		}

		public void onRegisterOk() {
			String text = "注册成功";
			showTips(text);
		}

		public void onRegisterFail(String reason) {
			String text = "注册失败，原因：" + reason;
			showTips(text);
		}

		public void onSettingHostOk(String new_host) {
			String text = "服务器地址已更新为：" + new_host;
			showTips(text);
		}

		public void onSettingHostError(String reason) {
			String text = "服务器地址设置失败，原因:" + reason;
			showTips(text);
			et__server_host.setError(text);
		}

		public void onSettingPortOk(String new_post) {
			String text = "服务器端口号已更新为：" + new_post;
			showTips(text);
		}

		public void onSettingPortError(String reason) {
			String text = "服务器端口号设置失败，原因:" + reason;
			showTips(text);
			et__server_port.setError(text);
		}

		public void onError(String reason) {
			String text ="错误：" + reason;
			showTips(text);
		}

		@Override
		public void onDebug(String reason) {
			String text = "调试信息：" + reason;
			showTips(text);
		}

		public void onServerConnOK(String reason) {
			String text = "服务器连接成功";
			showTips(text);
			if (actual_username!=null && actual_password!=null) {
				Log2.d("请求登录");
				ua_binder.login(actual_username, actual_password);
			}
		}

		public void onServerConnFail(String reason) {
			String text ="服务器连接失败，原因：" + reason;
			showTips(text);
		}

		public void onServerConnAbort(String reason) {
			String text =  "服务器连接中断，原因：" + reason;
			showTips(text);

		}

		public void onServerConnClose(String reason) {
			String text = "服务器已关闭连接，原因：" + reason;
			showTips(text);
		}

		@Override
		public void onServerDisconnected(String reason) {
			String text = "连接已断开，原因：" + reason;
			showTips(text);
		}

		@Override
		public void onServerConnTrying(String reason) {
			String text = "正在连接服务器";
			showTips(text);
		}

		@Override
		public void onServerConnected(String reason) {
			String text = "已连接到服务器";
			showTips(text);
		}

		private void showTips(String text) {
			Log2.d(text);
			if (this.toast2 != null) {
				toast2.setText(text);
				toast2.show();
			}
		}
	}

	private void attemptLogin() {
//		//防止重复创建登录任务
//		if (is_doing_login) {
//			return;
//		}

		// Reset errors.
		et__username.setError(null);
		et__password.setError(null);

		// 获取输入的用户名和密码
		String username = et__username.getText().toString();
		String password = et__password.getText().toString();
		String hostname = et__server_host.getText().toString();
		String port_str = et__server_port.getText().toString();

		View focusView = null;

		//校验必须的字段
		try {
			focusView = et__username;
			checker.check(et__username, username, Checker.TARGET_USERNAME);
			focusView = et__password;
			checker.check(et__password, password, Checker.TARGET_PASSWORD);
			focusView = et__server_host;
			checker.check(et__server_host, hostname, Checker.TARGET_HOSTNAME);
			focusView = et__server_port;
			checker.check(et__server_port, port_str, Checker.TARGET_PORT);
		} catch (Exception e) {
			focusView.requestFocus();
			return;
		}

		is_doing_login = true;

		if (ua_binder == null) {
			Log2.d("Chat", "错误: Login Activity ua_binder==null");
			return;
		}
		ua_binder.login(username, password);
	}

	private void attemptRegister(String username, String password) {
		if (ua_binder == null) {
			Log2.d("Chat", "错误: Login Activity ua_binder==null");
			return;
		}
		ua_binder.register(username, password);
	}

	private void onSettingChanging(){
		String hostname = et__server_host.getText().toString();
		String port_str = et__server_port.getText().toString();
		//变更默认服务器
		if (port_str != null && !port_str.isEmpty()) {
			actual_port = Integer.parseInt(port_str);
		}
		if (hostname == null || !hostname.isEmpty()) {
			actual_host = hostname;
		}

		Log2.d("hostname:"+actual_host + "  port:" + actual_port);
		ua_binder.setServer(actual_host, actual_port);
	}

	/**
	 * 表单字段检查器。
	 * 与{@link top.chenqs.mychat.common.models.Validator}的区别在于，
	 * 检查器只负责检查行为本身
	 * 验证器只负责字段的约束性校验
	 */
	class Checker {
		/**
		 * 检查用户名
		 */
		static public final int TARGET_USERNAME = 0;
		/**
		 * 检查密码
		 */
		static public final int TARGET_PASSWORD = 1;
		/**
		 * 检查主机名（IPv4地址）
		 */
		static public final int TARGET_HOSTNAME = 2;
		/**
		 * 检查端口号
		 */
		static public final int TARGET_PORT = 3;

		/**
		 * 使用指定的target检查value是否符合约束。
		 * 如果不符合，在对应的view上显示错误提示，并抛出异常
		 *
		 * @param view   用户显示错误提示的EditText
		 * @param value  待检查的值
		 * @param target 检查的目标, 取值为TARGET_USERNAME TARGET_PASSWORD TARGET_HOSTNAME TARGET_PORT之一
		 * @throws Exception 异常消息中保存了异常的原因。
		 */
		public void check(EditText view, String value, int target) throws Exception {
			assert view != null;
			String error_message = null;
			switch (target) {
			case TARGET_USERNAME:
				error_message = checkUsername(value);
				break;
			case TARGET_PASSWORD:
				error_message = checkPassword(value);
				break;
			case TARGET_HOSTNAME:
				error_message = checkHostname(value);
				break;
			case TARGET_PORT:
				error_message = checkPort(value);
				break;
			}

			//如果有错误消息时，则提示用户，并抛出异常
			if (error_message != null) {
				view.setError(error_message);
				throw new Exception(error_message);
			}
		}

		/**
		 * 检查两个编辑框的内容是否一致，不一致则抛出异常。
		 * @param view1
		 * @param value1
		 * @param view2
		 * @param value2
		 * @throws Exception
		 */
		public void check(EditText view1, String value1, EditText view2, String value2) throws Exception{
			if (view1 == null || value1 == null || view2 == null || value2 == null) {
				throw new Exception();
			}
			if (!value2.equals(value1)) {
				view2.setError(getString(R.string.error__different_password));
				throw new Exception();
			}

		}

		// 检查用户名格式有效性
		private String checkUsername(String username) {
			if (TextUtils.isEmpty(username)) {
				return getString(R.string.error__field_required);
			} else if (!Validator.isUsernameValid(username)) {
				return getString(R.string.error__invalid_username);
			}
			return null;
		}

		// 检查密码格式的有效性
		private String checkPassword(String password) {
			if (TextUtils.isEmpty(password)) {
				return getString(R.string.error__field_required);
			} else if (!Validator.isPasswordValid(password)) {
				return getString(R.string.error__invalid_password);
			}
			return null;
		}

		//检查主机名是否为ip地址或域名
		private String checkHostname(String hostname) {

			if (TextUtils.isEmpty(hostname))
				return null;
			if (!Validator.isHostnameValid(hostname)) {
				return getString(R.string.error__invalid_hostname);
			}
			return null;
		}

		//检查端口号是否有效
		private String checkPort(String port) {
			if (TextUtils.isEmpty(port))
				return null;
			try {
				int port_num = Integer.parseInt(port);
				if (!Validator.isPortValid(port_num)) {
					return getString(R.string.error__invalid_port);
				}
			} catch (NumberFormatException e) {
				return getString(R.string.error__invalid_port);
			}

			return null;
		}
	}

	/**
	 * 同时在控制adb和手机屏幕显示日志
	 * @param text
	 */
	private void log(String text) {
		if (text != null) {
			Log2.d(text);
			Toast.makeText(this, text, Toast.LENGTH_LONG).show();
		}
	}
}

