package top.chenqs.mychat.client.portable.enums;

/**
 * Created by chenqs on 2016/6/25.
 */
public class MessageType {
	public static final int ERROR = -1;
	public static final int DEBUG = -2;

	public static final int LOGIN_OK = 100;
	public static final int LOGIN_FAIL = 101;
	public static final int LOGIN_FAIL_PWD_ERROR = 102;
	public static final int LOGIN_TRYING = 103;

	public static final int REGISTER_OK = 110;
	public static final int REGISTER_FAIL = 111;
	public static final int REGISTER_FAIL_EXISTENT_USER = 112;

	public static final int SEND_CHAT_MSG_OK = 200;
	public static final int SEND_CHAT_MSG_FAIL = 201;

	public static final int RECV_CHAT_MSG_OK = 301;
	public static final int RECV_CHAT_MSG_FAIL = 302;

	public static final int FEEDBACK_SUCCESS = 400;
	public static final int FEEDBACK_FAILURE = 400;

	public static final int NET_CONNECTING = 400;
	public static final int NET_CONNECTED = 400;
	public static final int NET_DISCONNECTED = 401;

	public static final int SERVER_CONN_OK = 500;
	public static final int SERVER_CONN_FAIL = 501;
	public static final int SERVER_CONN_ABORT = 502;
	public static final int SERVER_CONN_TRYING = 503;
	public static final int SERVER_CONN_CLOSE = 504;

	@Deprecated
	public static final int SERVER_CONNECTED = 600;
	@Deprecated
	public static final int SERVER_CONNECTING = 601;
	@Deprecated
	public static final int SERVER_DISCONNECTED = 602;

	public static final int SETTING_HOSTNAME_OK = 700;
	public static final int SETTING_HOSTNAME_FAIL = 701;
	public static final int SETTING_PORT_OK = 710;
	public static final int SETTING_PORT_FAIL = 711;
	public static final int SETTING_TRYING = 720;
	public static final int SETTING_TRYING_FAIL = 721;

}
