package top.chenqs.mychat.common.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import top.chenqs.mychat.common.beans.SendingMessage;
import top.chenqs.mychat.common.beans.UserName;

/**
 * Created by chenqs on 2016/7/3.
 * 接收到的聊天消息
 */
public class ChatMessage {
	private String sender;	//消息的发送者
	private String content;	//消息的内容
	private Date time;	//消息发送者发送时的时间
	public ChatMessage(SendingMessage sending) {
		if (sending == null) {
			throw new RuntimeException("无法构造接收到的聊天消息，参数sending错误");
		}
		this.sender = sending.getSender().getUserName();
		this.content = sending.getPayLoad();
		this.time = new Date(sending.getSenderTimer());
	}

	public class Spec {
		private String content;
		public Spec(String content) {
			this.content = content;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}
	}

	/**
	 * 从待发送聊天消息中提取接收者列表。
	 * <p>注意：正确的消息形如 "@张三 @李四 @王五 消息正文"
	 *
	 * @param content   完整的消息原文，输入参数
	 * @param user_list 提取出来的接收者列表，输出参数，不能为null
	 */
	static public void extractReceivers(String content,
					    List<UserName> user_list) {
		if (content == null || content.isEmpty()) {
			return;
		}
		assert user_list != null;

		int i = content.indexOf('@', 0);
		int end = content.indexOf(' ', i);
		int last = 0;   //用于查找正文的开始位置
		while (i != -1 && end != -1) {
			last = end;
			String user = content.substring(i + 1, end);
			if (!user.isEmpty()) {
				user_list.add(new UserName(user));
			}
			i = content.indexOf('@', end);
			end = content.indexOf(' ', i);
		}

		return;

	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
}
