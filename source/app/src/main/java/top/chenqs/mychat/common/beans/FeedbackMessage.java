package top.chenqs.mychat.common.beans;

import com.alibaba.fastjson.annotation.JSONType;

/**
 * Created by chenqs on 2016/6/24.
 */
@JSONType(orders = {"protoSign", "msgLength", "msgType" , "senderTimer", "actionStatus"})
public class FeedbackMessage extends BasePDU {

        //反馈消息
        protected ActionStatus actionStatus;

        public FeedbackMessage() {
        }

        public FeedbackMessage(ActionStatus actionStatus) {
                super(10, 2);
                this.actionStatus = actionStatus;
        }

        public ActionStatus getActionStatus() {
                return actionStatus;
        }

        public void setActionStatus(ActionStatus actionStatus) {
                this.actionStatus = actionStatus;
        }

        @Override
        public String toString() {
                return "FeedbackMessage{" +
                    super.toString() + '\'' +
                    ", actionStatus=" + actionStatus +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                if (!super.equals(o)) return false;

                FeedbackMessage that = (FeedbackMessage) o;

                return actionStatus.equals(that.actionStatus);

        }

        @Override
        public int hashCode() {
                int result = super.hashCode();
                result = 31 * result + actionStatus.hashCode();
                return result;
        }
}
