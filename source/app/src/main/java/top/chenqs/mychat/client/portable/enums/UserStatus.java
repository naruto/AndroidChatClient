package top.chenqs.mychat.client.portable.enums;

/**
 * Created by chenqs on 2016/6/29.
 */
public enum UserStatus {
        OFFLINE, ONLINE
}
