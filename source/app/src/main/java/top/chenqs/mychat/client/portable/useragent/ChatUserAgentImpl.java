package top.chenqs.mychat.client.portable.useragent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import top.chenqs.mychat.client.portable.displayer.MessageDisplayer;
import top.chenqs.mychat.client.portable.enums.MessageType;
import top.chenqs.mychat.client.portable.enums.NetStatus;
import top.chenqs.mychat.client.portable.enums.UserStatus;
import top.chenqs.mychat.client.portable.io.MessageTransreceiver;
import top.chenqs.mychat.common.beans.FeedbackMessage;
import top.chenqs.mychat.common.beans.SendingMessage;
import top.chenqs.mychat.common.models.ChatMessage;
import top.chenqs.mychat.common.models.LoginUser;
import top.chenqs.mychat.common.models.PDUContainer;
import top.chenqs.mychat.common.beans.UserName;
import top.chenqs.mychat.common.models.PDUFactory;
import top.chenqs.mychat.common.models.RegisterUser;
import top.chenqs.mychat.common.models.User;

/**
 * Created by chenqs on 2016/6/26.
 * 聊天客户端，针对聊天客户端接口的实现。
 */
public class ChatUserAgentImpl implements ChatUserAgent {

	public static final String DEFAULT_HOST = "qnear.com";
	public static final int DEFAULT_PORT = 6666;
	public static final int MAX_CONN_FAIL_COUNT = 10;//失败重连最多次数

	//状态字段
	private boolean is_valid_config = false;
	private boolean is_alive_server = false;
	private boolean is_running = false;
	private boolean request_connect = false;	//请求进行连接
	private boolean request_login	= false;	//请求进行登陆
	private boolean request_reconnect = false;	//请求重新连接
	private boolean setting_changed = false;
	private int 	state = UAState.CFG_INVALID;
	private int	conn_fail_count = MAX_CONN_FAIL_COUNT;	//失败重连计数

	//基础设施
	private MessageTransreceiver transreceiver;	//消息转发器
	private MessageDisplayer displayer;
	Queue<ChatMessage> chat_msg_queue = new ConcurrentLinkedQueue<>();	//新聊天消息队列
	Queue<Task> task_queue = new ConcurrentLinkedQueue<>();			//发送任务队列
	Thread recv_t = new Thread() {		//负责接收消息的线程
		@Override
		public void run() {
			BufferedReader reader;
			try  {
				onReceiving();
			}catch (IOException e) {
				onIOException();
			}
		}
	};

	//配置字段
	private InetSocketAddress server_addr;
	private Socket socket = new Socket();
	private User me;
	private String server_host = DEFAULT_HOST;
	private int server_port = DEFAULT_PORT;

	//其它字段
	private List<UserName> recent_user_list;	//最近目标接收用户列表

	class Task {
		public static final int SET_PORT = 0;
		public static final int SET_HOST = 1;
		public static final int SEND_MSG = 2;
		private String pdu;
		private int action;

		public Task(String pdu) {
			this.pdu = pdu;
		}

		public String getPdu() {
			return pdu;
		}

		public void setPdu(String pdu) {
			this.pdu = pdu;
		}

		public int getAction() {
			return action;
		}

		public void setAction(int action) {
			this.action = action;
		}
	}

	public ChatUserAgentImpl(MessageDisplayer displayer) {
		assert displayer != null : "displayer不能为null，请联系开发者";
		this.displayer = displayer;
		this.transreceiver = new MessageTransreceiver(displayer);
	}

	public void start() {

	}

	public void stop() {
		is_running = false;
	}

	private static class UAState {
		public static final int CFG_INVALID 	= 0;
		public static final int CFG_OK		= 1;
		public static final int CONN_TRYING	= 2;
		public static final int CONN_OK		= 3;
		public static final int CONN_CLOSE	= 4;
		public static final int USER_OFFLINE	= 5;
		public static final int USER_ONLINE	= 6;
		public static final int USER_LOGIN 	= 7;

	}

	private void onConfigInvalid() {
		if (setting_changed) {
			onSettingChanged();
		}
	}
	private void onConfigOK(){
		if (request_connect) {
			state = UAState.CONN_TRYING;
			conn_fail_count = MAX_CONN_FAIL_COUNT;
		} else if (request_reconnect) {
			state = UAState.CONN_TRYING;
		}

	}
	private void onTryingConnect(){
		while (0 < conn_fail_count--) {
			int i = MAX_CONN_FAIL_COUNT - conn_fail_count;
			transreceiver.transfer(MessageType.SERVER_CONN_TRYING, "正在尝试第" + i + "次连接。。" );
			if (doConnectToServer(this.server_addr)) {
				state = UAState.CONN_OK;
				break;
			} else {
				state = UAState.CFG_OK;
			}
		}
		request_connect = false;
	}

	private void onConnOK() {
		if (request_login) {
			state = UAState.USER_LOGIN;
		}
	}

	private void onConnClosed() {
		transreceiver.transfer(MessageType.SERVER_CONN_CLOSE, "连接已关闭");
	}

	private void onReConnect() {} {

	}

	private void onReLogin(){}

	private void onUserLogin(){
		if (me == null) {
			transreceiver.transfer(MessageType.LOGIN_FAIL, "程序内部错误");
			return;
		}
		try {
			String reason = doUserLogin(me);
			if (reason == null) {
				transreceiver.transfer(MessageType.LOGIN_OK, me.getUsername());
				state = UAState.USER_ONLINE;
				me.setStatus(UserStatus.OFFLINE);
				request_reconnect = false;
				request_login = false;
				request_connect = false;
			} else {
				//登录失败，或超过重连次数
				transreceiver.transfer(MessageType.LOGIN_FAIL, reason);
				state = UAState.USER_OFFLINE;
				request_reconnect = false;	//不再尝试重连
				request_login = false;
				request_connect = false;
			}

		} catch (SocketTimeoutException e){
			//超时重连一定的次数
			String text =  "读取数据超时，服务器没有返回任何数据";
			transreceiver.transfer(MessageType.LOGIN_FAIL, text);
			me.setStatus(UserStatus.OFFLINE);
			state = UAState.CFG_OK;
			request_reconnect = true;	//尝试重连
			request_login = true;
			request_connect = false;

		} catch (IOException e) {
			transreceiver.transfer(MessageType.ERROR, e);
		}

	}
	private void onUserOnline(){
		//启动一个新线程在后台负责接收消息，防止没有读到数据时，阻塞UA线程，导致无法及时发送消息。

		recv_t.start();

		try {
			onSending();
		} catch (IOException e) {
			onIOException();
		}
	}
	private void onUserOffline(){
		if (me.isOnline()) {
			me.setOffline();
			transreceiver.transfer(MessageType.LOGIN_FAIL, "用户处于离线状态");
		}
	}

	private void onReceiving() throws IOException{
		//接受数据，转发消息给前台界面
		InputStream input = socket.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		while (is_running) {
			String pdu = reader.readLine();
			PDUContainer container = new PDUContainer();
			PDUFactory.parsePDU(pdu, container);
			transferPDU(container);
		}
	}

	private void onIOException() {
		transreceiver.transfer(MessageType.SEND_CHAT_MSG_FAIL, "网络IO异常");
		me.setOffline();
		if (socket.isClosed()) {
			transreceiver.transfer(MessageType.SERVER_CONN_CLOSE, "无");
			request_connect = request_login = true;
			state = UAState.CONN_TRYING;
		} else {
			request_login = true;
			state = UAState.CONN_OK;
		}
	}
	private void onSending() throws IOException{
		OutputStream output = socket.getOutputStream();
		while (is_running) {
			Task task = task_queue.poll();
			if (task == null) {
				//如果task为null，说明没有需要发送的数据
				continue;
			}
			String pdu = task.getPdu();
			output.write(pdu.getBytes("UTF-8"));
			output.flush();
			transreceiver.transfer(MessageType.SEND_CHAT_MSG_OK, (String)null);
		}
	}
	@Override
	public void run() {
		is_running = true;

		while (is_running) {
			switch (state) {
			case UAState.CFG_INVALID:
				onConfigInvalid();
				break;
			case UAState.CFG_OK:
				onConfigOK();
				break;
			case UAState.CONN_TRYING:
				onTryingConnect();
				break;
			case UAState.CONN_OK:
				onConnOK();
				break;
			case UAState.CONN_CLOSE:
				onConnClosed();
				break;
			case UAState.USER_ONLINE:
				onUserOnline();
				break;
			case UAState.USER_OFFLINE:
				onUserOffline();
				break;
			case UAState.USER_LOGIN:
				onUserLogin();
			}

		}
	}

	private synchronized void commitTask(String json) {
		transreceiver.transfer(MessageType.DEBUG, "提交任务");
		transreceiver.transfer(MessageType.DEBUG, json);
		task_queue.add(new Task(json));
	}

	/**
	 * 登录
	 * @param username 用户名
	 * @param password 密码（明文）
	 * @return 保留
	 * @throws Exception 当网络不正常或者没有连接到服务器时，会抛出异常
	 */
	@Override
	public int login(String username, String password) throws Exception {
		if (username == null) {
			transreceiver.transfer(MessageType.LOGIN_TRYING, "用户名不能为空。登录失败");
			return -1;
		}
		User u = new LoginUser(username, password);
		me = u;
		return dologin(u);
	}

	/**
	 * 登录或注册
	 * @param u 封装了用户信息的用户对象
	 */
	private int dologin(final User u) throws Exception {
		//创建用户，生成协议文本，提交到任务队列
//		checkNetStatus();
		if (u==null) {
			transreceiver.transfer(MessageType.ERROR, "登录user参数不能为null");
			return -1;
		}

		prepareConfig();
		prepareServer();

		if (u.isOffline()) {
			transreceiver.transfer(MessageType.LOGIN_TRYING, "正在登录");
			String pdu = PDUFactory.createLoginPDU(u);      //生成协议文本
			commitTask(pdu);       //提交到任务队列
			request_connect = true;
			request_login = true;
		}

		return 0;
	}

	/**
	 * 注册用户
	 *
	 * @param username 用户名
	 * @param password 密码（明文）
	 * @return 保留
	 * @throws Exception 当网络不正常或者没有连接到服务器时，会抛出异常
	 */
	@Override
	public synchronized int register(String username, String password) throws Exception {
		//创建用户，生成协议文本，发送协议文本到服务器，
		//接收反馈，解析反馈信息，根据反馈信息进行相应的处理

		User u = new RegisterUser(username, password);
		String pdu = PDUFactory.createRegisterPDU(u);
		commitTask(pdu);
		me = u;
		int result = dologin(me);
		me.setPassword(null);
		return result;
	}

	@Override
	public int logout() {
		return 0;
	}

	/**
	 * 没有指定服务器ip或主机名和端口号时，将使用默认服务器。
	 */
	public void useDefaultServer() {
		setServer(DEFAULT_HOST, DEFAULT_PORT);
	}

	@Override
	public void setServer(String host, int port) {
		transreceiver.transfer(MessageType.DEBUG, "正在设置服务器");
		is_valid_config = false;

		if (host == null || host.isEmpty()) {
			transreceiver.transfer(MessageType.ERROR, "主机不能为空");
			return;
		}
		if (port < 0 || port > 65535) {
			transreceiver.transfer(MessageType.ERROR, "端口号超出范围(0 ~ 65535)");
			return;
		}

		this.server_host = host;
		this.server_port = port;
		this.setting_changed = true;
	}

	/**
	 * 检查网络状态
	 *
	 * @return 当前的网络状态
	 * @throws Exception 当网络不正常时会抛出异常
	 */
	private NetStatus checkNetStatus() throws Exception {
		return NetStatus.UNKNOWN;
	}

	/**
	 * 为网络访问任务准备好服务器连接
	 *
	 * @throws Exception 当没有正常连接到服务器时，会抛出异常
	 */
	private void prepareServer() throws Exception {
		//如果服务器未连接则请求进行连接，如果已连接，直接返回。如果3次尝试连接失败，则抛出异常。
		if (isAliveServer()) {
			return;
		}
	}

	/**
	 * 为网络访问任务准备好服务器配置
	 * @throws Exception
	 */
	private void prepareConfig() throws Exception {
		if (isValidConfig()) {
			return;
		}
		if (this.server_addr == null) {
			useDefaultServer();
		} else {
			throw new Exception("无效的服务器设置");
		}
	}

	/**
	 * 检查用户账号的状态
	 *
	 * @throws Exception 当没有登录时，将抛出异常
	 */
	private void checkUserStatus() throws Exception {
		if (me == null || me.isOffline())
			throw new Exception("请先登录");
	}

	/**
	 * 连接到服务器
	 * @param address 经过正确初始化的服务器地址
	 * @return 连接成功或者失败
	 */
	private boolean doConnectToServer(SocketAddress address) {
		if (!is_valid_config || address == null) {
			transreceiver.transfer(MessageType.ERROR,
			    new Exception("用户代理没有正确初始化，无法进行下一步工作"));

			return false;
		}
		if (isAliveServer()) {
			transreceiver.transfer(MessageType.SERVER_CONN_OK, "服务器已连接");
			return true;
		}

		try {
			socket.connect(address, 5000);
			if (socket.isConnected()) {
				transreceiver.transfer(MessageType.SERVER_CONN_OK, "服务器连接成功");
				return true;
			} else  {
				transreceiver.transfer(MessageType.SERVER_CONN_FAIL,
				    "无法连接到服务器：" + address.toString());
				return false;
			}
		} catch (IOException e) {
			transreceiver.transfer(MessageType.SERVER_CONN_FAIL,
			    "无法连接到服务器：" + address.toString());
			transreceiver.transfer(MessageType.ERROR, e);
			return false;
		}

	}

	/**
	 * 执行登录操作
	 * @return 登录成功则返回null，登录失败则返回失败原因。
	 */
	private String doUserLogin(User u) throws IOException {
		//构造PDU，发送，然后等待反馈，再处理反馈
		String pdu = PDUFactory.createLoginPDU(u);
		try {
			OutputStream out = socket.getOutputStream();
			InputStream in = socket.getInputStream();
			out.write(pdu.getBytes("UTF-8"));
			out.flush();
			socket.setSoTimeout(2000);
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String data = reader.readLine();
			FeedbackMessage feedback = PDUFactory.parsePDU(data, FeedbackMessage.class);
			if (feedback!=null && feedback.getActionStatus().getActionRslCode() == 0) {
				//登录成功
				return null;
			}
			if (feedback == null) {
				return "无法识别的反馈消息";
			} else {
				return feedback.getActionStatus().getActionRslMsg();
			}


		} catch (SocketTimeoutException e){
			if (conn_fail_count > 0) {
				throw e;
			} else {
				return "超过最大允许的重连次数";
			}

		}  catch (IOException e) {
			throw e;
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	/**
	 * 对收到的协议数据单元进行转发
	 *
	 * @param container     [输入参数] 承载协议数据单元的容器
	 */
	private void transferPDU(PDUContainer container) {
		if (container == null || transreceiver == null)
			return;
		if (container.hasSending()) {
			//如果是聊天消息，则转储到聊天消息队列，通知前台界面来取数据。
			SendingMessage sending = container.getSending();
			chat_msg_queue.add(new ChatMessage(sending));
			transreceiver.transfer(sending);
		} else if (container.hasFeedback()) {
			//如果是反馈消息，则交由转发器自行处理。
			transreceiver.transfer(container.getFeedback());
		}
		if (container.hasException()) {
			//如果发生异常，则告知用户
			transreceiver.transfer(MessageType.ERROR, container.getException());
		}
	}

	/**
	 * 保存最近指定的目标用户
	 *
	 * @param user_list 目标用户列表
	 */
	private void saveRecentUserList(List<UserName> user_list) {
		if (user_list != null && user_list.size() != 0)
			this.recent_user_list = user_list;
	}

	@Override
	public void connectServer() {
		request_connect = true;
	}

	/**
	 * 发送聊天消息
	 *
	 * @param content 聊天消息的原文
	 * @throws Exception
	 */
	@Override
	public void sendMessage(String content) throws Exception {
		List<UserName> user_list = new ArrayList<>();
		ChatMessage.extractReceivers(content, user_list);	//提取接收者列表
		saveRecentUserList(user_list);
		if (this.recent_user_list == null) {
			throw new Exception("您还没有指定需要接收消息的用户");
		}
		String pdu = PDUFactory.createChatPDU(me, this.recent_user_list, content);
		commitTask(pdu);
	}

	/**
	 * 检查聊天消息队列中是否存在尚未取出的消息。
	 *
	 * @return 若存在未读消息，则返回true，否则返回false
	 */
	@Override
	public boolean hasNextMessage() {
		return !chat_msg_queue.isEmpty();
	}

	/**
	 * 获取最新消息
	 * @return 新消息，或null
	 */
	@Override
	public ChatMessage fetchMessage() {
		return chat_msg_queue.poll();
	}

	@Override
	public void disconnectServer() {
		try {
			if (!socket.isClosed())
				socket.close();
		} catch (IOException e) {
			transreceiver.transfer(MessageType.ERROR,
			    new Exception("关闭连接时出现发生异常"));
		}

	}

	public void onSettingChanged() {
		transreceiver.transfer(MessageType.DEBUG, "准备重新设置服务器");
		InetSocketAddress addr = new InetSocketAddress(server_host, server_port);
		if (addr == null) {
			transreceiver.transfer(MessageType.DEBUG, "无法设置服务器");
			return;
		}
		if (addr.isUnresolved()) {
			transreceiver.transfer(MessageType.DEBUG, "无法解析的服务器地址");
			transreceiver.transfer(MessageType.ERROR, "无法解析的服务器地址：" + addr.toString());
			is_valid_config = false;
		}
		else {
			transreceiver.transfer(MessageType.DEBUG, "已重新设置服务器");
			transreceiver.transfer(MessageType.SETTING_HOSTNAME_OK, addr.toString());
			transreceiver.transfer(MessageType.SETTING_PORT_OK, String.valueOf(addr.getPort()));
			this.server_addr = addr;
			this.state = UAState.CFG_OK;
			is_valid_config = true;
		}
		this.setting_changed = false;

	}

	/**
	 * 关闭客户端
	 */
	@Override
	public void close() {
		if (!socket.isClosed())
			disconnectServer();
	}

	@Override
	public void displayMessage(int what, String content) {
		transreceiver.displayMessage(what, content);
	}

	/*============================== 判断 ===============================*/

	@Override
	public boolean isValidConfig() {
		is_valid_config = this.server_addr!=null && !this.server_addr.isUnresolved();
		return is_valid_config;
	}

	/**
	 * 套接字是否可用
	 * @return
	 */
	@Override
	public boolean isAliveServer() {
		//return this.server_status == ServerStatus.CONNECTED;
		//return socket.isConnected();
		return socket != null && socket.isConnected();
	}

	/**
	 * 是否已登陆
	 * @return 是否已登陆
	 */
	@Override
	public boolean isOnlineUser() {
		return this.me != null && this.me.isOnline();
	}

	public boolean isRunning() {
		return false;
	}
}
