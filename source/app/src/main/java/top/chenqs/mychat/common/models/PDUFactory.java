package top.chenqs.mychat.common.models;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import top.chenqs.mychat.common.beans.FeedbackMessage;
import top.chenqs.mychat.common.beans.SendingMessage;
import top.chenqs.mychat.common.beans.UserName;

/**
 * Created by chenqs on 2016/6/29.
 * 协议数据单元创建者
 * PDUFactory :协议数据单元 Protocol Data Unit
 */
public class PDUFactory {
	static public String linesep;
	static {
		linesep = System.getProperty("line.separator");
	}

	/**
	 * 生成注册用的PDU
	 */
	static public String createRegisterPDU(User user) {
                assert user != null : "参数user不能为null";
                List<UserName> target_users = new ArrayList<>();
		return doCreatePDU(user,target_users, "");
        }

	/**
	 * 生成登录用的PDU
	 */
	static public String createLoginPDU(User user) {
                assert user != null : "参数user不能为null";
                List<UserName> target_users = new ArrayList<>();
                return doCreatePDU(user,target_users, "");
        }

	/**
	 * 实际创建PDU
	 * @param user
	 * @param target_users
	 * @param content
	 * @return
	 */
	static private String doCreatePDU(User user, List<UserName> target_users, String content) {
		SendingMessage msg = new SendingMessage(user.getSender(),  content, "s", target_users);
		String pdu = JSON.toJSONString(msg);
		pdu = escape(pdu) + linesep;
		return pdu;
	}

        /**
         * 生成聊天用的PDU
         * @param self 聊天消息发送者自身
         * @param target_users 聊天接收消息的目标用户
         * @param content 聊天消息原文
         * @return 聊天用的PDU
         */
        static public String createChatPDU(User self, List<UserName> target_users,
					   String content)
        {
                assert target_users != null;
                assert self != null;
                assert content != null;

                return doCreatePDU(self, target_users, content);
        }

	/**
	 * 解析返回的协议数据单元，反序列化成一个对象。
	 * @param pdu 返回的协议数据单元
	 * @param clazz 反序列化的目标类
	 * @param <T> 反序列化的目标类型
	 * @return 反序列化后的对象
	 * @throws Exception pdu为null时抛出异常
	 */
        static public <T> T parsePDU(String pdu, Class<T> clazz) throws Exception{
		if (pdu == null) {
			throw new Exception("非法pdu，参数不能为null");
		}
		String s = unescape(pdu);
		T t = null;
		try {
			t = JSON.parseObject(s, clazz);
			return t;
		} catch (Exception e) {
			return null;
		}
        }

        /**
         * 解析协议数据，并将其封装到协议数据单元容器里。
	 * @param pdu	[输入参数] 协议数据单元，不能为null
	 * @param container [输出参数] 协议数据单元容器，不能为null
	 */
	static public void parsePDU(String pdu, PDUContainer container) {
		if (pdu == null || container == null)
			return;
		try {
			SendingMessage sending = parsePDU(pdu, SendingMessage.class);
			container.setSending(sending);
		} catch (Exception e1) {
			try {
				FeedbackMessage feedback = parsePDU(pdu, FeedbackMessage.class);
				container.setFeedback(feedback);
			} catch (Exception e2) {

			}
		}

	}

	static public String escape(String s) {
		String s2 = s.replace("\\", " \\\\ ");
		s2 = s2.replace(linesep, " \\n ");
		return s2;
	}

	static public String unescape(String s) {
		String s2 = s.replace(" \\n ", linesep);
		s2 = s2.replace(" \\\\ ", "\\");
		return s2;
	}

}
