package top.chenqs.mychat.common.reactor;

/**
 * Created by chenqs on 2016/7/3.
 */
public class MessageSpec {
	private int what;
	private String reason;

	public MessageSpec() {
	}

	public MessageSpec(int what, String reason) {
		this.what = what;
		this.reason = reason;
	}

	public int getWhat() {
		return what;
	}

	public void setWhat(int what) {
		this.what = what;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
