package top.chenqs.mychat.client.portable.displayer;

import java.security.MessageDigest;

/**
 * Created by chenqs on 2016/6/29.
 */
public class ConsoleMessageDisplayer implements MessageDisplayer {
        @Override
        public void showMessage(int type, String content) {
                System.out.println("消息类型:" + type + "， 消息正文:"+ content);
        }
}
