package top.chenqs.mychat.client.platform.android.displayer;

import android.content.Context;
import android.content.Intent;

import top.chenqs.mychat.client.portable.displayer.MessageDisplayer;
import top.chenqs.mychat.client.portable.enums.MessageType;

/**
 * Created by chenqs on 2016/7/2.
 */
public class BroadcastMessageDisplayer implements MessageDisplayer {
	private Intent broadcast_intent;
	private String action;
	private Context context;
	private boolean can_show_debug_info = true;

	public BroadcastMessageDisplayer(Context context, String action) {
		if (action != null)
			this.action = action;
		if (context != null) {
			this.context = context;
		}
	}

	@Override
	public void showMessage(int type, String content) {

		Intent broadcast_intent = new Intent(action);
		broadcast_intent.putExtra("what", type);
		if (type == MessageType.DEBUG && !can_show_debug_info) {
			return;
		}
		if (content != null) {
			broadcast_intent.putExtra("reason", content);
		}
		this.context.sendBroadcast(broadcast_intent);
	}
}
