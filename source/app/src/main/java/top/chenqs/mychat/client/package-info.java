/**
 * 该包是客户端专用的所有代码。
 * 包平台无关的包{@link top.chenqs.mychat.client.portable}，
 * 和平台相关的包{@link top.chenqs.mychat.client.platform}。
 * 此处的平台是指Android平台和Java平台。
 * 特别需要注意的是：所有引用了Android平台专用的包，
 * 都必须放在{@link top.chenqs.mychat.client.platform.android}包中。
 * @version 1.0
 */
package top.chenqs.mychat.client;