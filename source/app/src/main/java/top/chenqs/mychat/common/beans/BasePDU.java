package top.chenqs.mychat.common.beans;

import com.alibaba.fastjson.annotation.JSONType;

/**
 * 协议数据单元的公共部分
 * <p>Created by chenqs on 2016/6/24.
 */
public class BasePDU {
        /**
         * 协议头，固定值，次序、项目与内容都不能改变
         */
        protected int protoSign = 142857;          //:　142857

        //消息长度与0x40000000相“或”的值，如，消息长度为0x10，则
        //　　　该值为　0x10　|　0x40000000,　即，0x40000010
        protected int msgLength;

        /**
         * 2表示反馈消息
         */
        protected int msgType;            //:　2

        /**
         * 发送时间，以1970年1月1日　00:00:00开始的纳秒数
         */
        protected long senderTimer;       //:　1466038657122130200

        public BasePDU() {
                this(0, 2);
        }

        /**
         * 构造协议数据单元的公共部分。
         * @param msgLength 消息长度
         * @param msgType 消息类型，2表示反馈消息，22表示聊天消息
         */
        public BasePDU(int msgLength, int msgType) {
                this(msgLength, msgType, System.currentTimeMillis());
        }

        /**
         * 构造协议数据单元的公共部分。
         * @param msgLength 消息长度
         * @param msgType 消息类型
         * @param senderTimer 消息的产生时间
         */
        public BasePDU(int msgLength, int msgType, long senderTimer) {
                this.msgLength = msgLength | 0x40000000;
                this.msgType = msgType;
                this.senderTimer = senderTimer;
        }

        public int getProtoSign() {
                return protoSign;
        }

        public int getMsgLength() {
                return msgLength;
        }

        public void setMsgLength(int msgLength) {
                this.msgLength = msgLength | 0x40000000;
        }

        public int getMsgType() {
                return msgType;
        }

        public void setMsgType(int msgType) {
                this.msgType = msgType;
        }

        public long getSenderTimer() {
                return senderTimer;
        }

        public void setSenderTimer(long senderTimer) {
                this.senderTimer = senderTimer;
        }

        @Override
        public String toString() {
                return "BasePDU{" +
                    "protoSign=" + protoSign +
                    ", msgLength=" + msgLength +
                    ", msgType=" + msgType +
                    ", senderTimer=" + senderTimer +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                BasePDU that = (BasePDU) o;

                if (protoSign != that.protoSign) return false;
                if (msgLength != that.msgLength) return false;
                if (msgType != that.msgType) return false;
                return senderTimer == that.senderTimer;

        }

        @Override
        public int hashCode() {
                int result = protoSign;
                result = 31 * result + msgLength;
                result = 31 * result + msgType;
                result = 31 * result + (int) (senderTimer ^ (senderTimer >>> 32));
                return result;
        }
}
