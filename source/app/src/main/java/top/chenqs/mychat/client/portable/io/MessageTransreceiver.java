package top.chenqs.mychat.client.portable.io;

import top.chenqs.mychat.client.portable.displayer.MessageDisplayer;
import top.chenqs.mychat.client.portable.enums.MessageType;
import top.chenqs.mychat.common.beans.ActionStatus;
import top.chenqs.mychat.common.beans.FeedbackMessage;
import top.chenqs.mychat.common.beans.SendingMessage;

/**
 * Created by chenqs on 2016/7/3.
 * 消息收发器
 */
public class MessageTransreceiver {

	private MessageDisplayer displayer;

	public MessageTransreceiver(MessageDisplayer displayer) {
		if (displayer == null)
			throw new RuntimeException("MessageDisplayer不能为null，"
			    + "消息转发器无法正常构造");
		this.displayer = displayer;
	}

	public void transfer(FeedbackMessage feedback) {
		if (feedback == null) return;
		ActionStatus status = feedback.getActionStatus();
		String result = status.getActionRslMsg();
		int type = status.getActionRslCode() == 0 ?
		    MessageType.FEEDBACK_SUCCESS : MessageType.FEEDBACK_FAILURE;
		displayMessage(type, result);
	}

	public void transfer(SendingMessage sending) {
		if (sending == null) return;
		displayMessage(MessageType.RECV_CHAT_MSG_OK, null);
	}

	public void transfer(int type, Exception e) {
		displayMessage(type, e.getMessage());
	}

	public void transfer(int type, String content) {
		displayMessage(type, content);
	}

	public void displayMessage(int type, String content) {
		displayer.showMessage(type, content);
	}
}
