package top.chenqs.mychat.client.portable.enums;

/**
 * Created by chenqs on 2016/6/29.
 * 网络状态
 */
public enum  NetStatus {
        UNKNOWN, ONLINE, OFFLINE
}
