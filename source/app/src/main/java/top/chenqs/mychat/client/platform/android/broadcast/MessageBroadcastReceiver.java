package top.chenqs.mychat.client.platform.android.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import top.chenqs.mychat.client.portable.enums.MessageType;
import top.chenqs.mychat.client.platform.android.utils.Log2;

/**
 * 用于通知界面提取新消息的广播接收器
 */
public class MessageBroadcastReceiver extends BroadcastReceiver {
	public static final String ACTION
	    = "top.chenqs.mychat.client.platform.android.intent.action.MESSAGE";
	private Handler message_handler;

	public MessageBroadcastReceiver() {
	}

	public MessageBroadcastReceiver(Handler message_handler) {
		if (message_handler == null) {
			throw new RuntimeException(
			    "无法构造MessageBroadcastReceiver：" + "message_handler==null");
		}
		this.message_handler = message_handler;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Log2.d("Chat", "MessageBroadcastReceiver onReceive");
		if (message_handler != null) {
			Message msg = Message.obtain();
			Bundle bundle = intent.getExtras();
			if (bundle == null)
				throw new RuntimeException("登录信息广播状态字段what不能为null");
			msg.what = bundle.getInt("what");
			Log2.d("Chat", msg.what + " " + bundle.getString("reason"));
			msg.setData(bundle);
			message_handler.sendMessage(msg);
		}
	}
}