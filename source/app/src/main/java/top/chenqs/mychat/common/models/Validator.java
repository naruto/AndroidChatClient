package top.chenqs.mychat.common.models;

import android.support.v7.app.AppCompatActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by chenqs on 2016/7/2.
 */
public class Validator {
        static public boolean isUsernameValid(String username) {
                return username != null && username.length() > 0;
        }

        static public boolean isPasswordValid(String password) {
                int len = password.length();
                return len >= 6 && len <= 20;
        }

        static public boolean isHostnameValid(String host) {
                if (host == null || host.isEmpty())
                        return false;
                String ipv4 = "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$";
                Pattern pattern = Pattern.compile(ipv4);
                Matcher matcher = pattern.matcher(host);
                if (!matcher.matches())
                        return false;
                String[] array = host.split(".");
                try {
                        for (String item : array) {
                                int number = Integer.parseInt(item);
                                if (number < 0 || number > 255) {
                                        return false;
                                }
                        }
                        return true;
                } catch (Exception e) {
                        return false;
                }
        }

        static public boolean isPortValid(int port) {
                return 0 <= port && port <= 65535;
        }
}
