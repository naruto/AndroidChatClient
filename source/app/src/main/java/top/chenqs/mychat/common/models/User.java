package top.chenqs.mychat.common.models;

import java.security.MessageDigest;

import top.chenqs.mychat.client.portable.enums.NetStatus;
import top.chenqs.mychat.client.portable.enums.UserStatus;
import top.chenqs.mychat.common.beans.Sender;

/**
 * Created by chenqs on 2016/6/26.
 */
public class User {

        private Sender sender;
        private UserStatus status = UserStatus.OFFLINE;

        public User(String username, String password) {
                sender = new Sender(username, password);
        }

        public String getUsername() {
                return sender.getUserName();
        }

        public void setUsername(String username) {
                sender.setUserName(username);
        }

        public String getPassword() {
                return sender.getUserPWD();
        }

        public void setPassword(String password) {
                sender.setUserPWD(password);
        }

        public static String getMD5(String str) {
                if (str == null || str.isEmpty())
                        return null;
                MessageDigest digester = null;
                try {
                        digester = MessageDigest.getInstance("MD5");
                } catch (Exception e) {

                }

                if (digester == null) {
                        return str;
                }

                byte[] bytes = str.getBytes();
                digester.update(bytes);
                byte[] digest = digester.digest();

                return convertToHexString(digest);
        }

        private static String convertToHexString(byte[] bytes) {
                final char[] hex_digit =
                    {'0', '1', '2', '3',
                        '4', '5', '6', '7',
                        '8', '9', 'A', 'B',
                        'C', 'D', 'E', 'F'};
                char[] result = new char[bytes.length*2];
                int i = 0;
                for (byte b : bytes) {
                        result[i++] = hex_digit[b>>4 & 0xf];    //处理高四位
                        result[i++] = hex_digit[b & 0xf];       //处理低四位
                }
                return new String(result);
        }

        public Sender getSender() {
                return this.sender;
        }

        public void setSender(Sender sender) {
                this.sender = sender;
        }

        public boolean isOnline() {
                return status == UserStatus.ONLINE;
        }

        public boolean isOffline() {
                return status == UserStatus.OFFLINE;
        }

        public UserStatus getStatus() {
                return status;
        }

        public void setStatus(UserStatus status) {
                this.status = status;
        }

        public void setOnline() {
                this.setStatus(UserStatus.ONLINE);
        }
        public void setOffline() {
                this.setStatus(UserStatus.OFFLINE);
        }

        @Override
        public String toString() {
                return "User{" +
                    "sender=" + sender.toString() +
                    '}';
        }
}
