/**
 * 该包存放Android平台客户端专用的UI相关代码。
 * 所有与Android UI相关的代码都必须放在该包及其子包下。
 * 不存放UI无关代码。原因是Android的UI操作不是线程安全的，需要进行隔离。
 * @version 1.0
 */
package top.chenqs.mychat.client.platform.android.ui;