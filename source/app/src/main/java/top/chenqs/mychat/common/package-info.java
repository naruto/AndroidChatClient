/**
 * 该包内的所有代码都是跨平台的、非安卓平台或Java平台专用的、
 * 并且在服务器和客户端都可能会用到的公共代码。
 * 一般来说，存放于业务处理相关的代码。
 * @version 1.0
 */
package top.chenqs.mychat.common;