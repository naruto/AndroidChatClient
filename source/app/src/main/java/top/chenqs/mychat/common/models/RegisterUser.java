package top.chenqs.mychat.common.models;

/**
 * Created by chenqs on 2016/7/9.
 */
public class RegisterUser extends User {
	public RegisterUser(String username, String password) {
		super(username, User.getMD5(password));
	}
}
