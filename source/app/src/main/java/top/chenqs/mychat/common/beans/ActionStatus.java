package top.chenqs.mychat.common.beans;

import com.alibaba.fastjson.annotation.JSONType;

/**
 * Created by chenqs on 2016/6/24.
 * 反馈消息
 */
@JSONType(orders = {"actionMsgType", "actionRslMsg", "actionRslCode"})
public class ActionStatus {
        //接收到的消息类型
        protected int actionMsgType;      //:　22

        //反馈的文本内容
        protected String actionRslMsg;    //:　"success"

        //0表示成功，其它值表示失败，失败原因放在actionRslMsg中
        protected int actionRslCode;      //:　0

        public ActionStatus() {
        }

        public ActionStatus(int actionMsgType, String actionRslMsg, int actionRslCode) {
                this.actionMsgType = actionMsgType;
                this.actionRslMsg = actionRslMsg;
                this.actionRslCode = actionRslCode;
        }

        public int getActionMsgType() {
                return actionMsgType;
        }

        public void setActionMsgType(int actionMsgType) {
                this.actionMsgType = actionMsgType;
        }

        public String getActionRslMsg() {
                return actionRslMsg;
        }

        public void setActionRslMsg(String actionRslMsg) {
                this.actionRslMsg = actionRslMsg;
        }

        public int getActionRslCode() {
                return actionRslCode;
        }

        public void setActionRslCode(int actionRslCode) {
                this.actionRslCode = actionRslCode;
        }

        @Override
        public String toString() {
                return "ActionStatus{" +
                    "actionMsgType=" + actionMsgType +
                    ", actionRslMsg='" + actionRslMsg + '\'' +
                    ", actionRslCode=" + actionRslCode +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                ActionStatus that = (ActionStatus) o;

                if (actionMsgType != that.actionMsgType) return false;
                if (actionRslCode != that.actionRslCode) return false;
                return actionRslMsg.equals(that.actionRslMsg);

        }

        @Override
        public int hashCode() {
                int result = actionMsgType;
                result = 31 * result + actionRslMsg.hashCode();
                result = 31 * result + actionRslCode;
                return result;
        }
}
