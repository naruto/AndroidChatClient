package top.chenqs.mychat.client.platform.android.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import top.chenqs.mychat.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kzz on 4/7/16.
 */
public class MsgViewAdapter extends BaseAdapter {

  private LayoutInflater mInflater;
  ArrayList<HashMap<String, Object>> data;


  public MsgViewAdapter(Context context, ArrayList<HashMap<String, Object>> arg_data){
    mInflater = LayoutInflater.from(context);
    data = arg_data;
  }

  @Override
  public int getCount() {
    return data.size();
  }

  @Override
  public Object getItem(int position) {
    return null;
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    RecyclerView.ViewHolder holder;
    if(convertView == null){
      convertView = mInflater.inflate(R.layout.msg_item_view,null);
    }
    //holder = new RecyclerView.ViewHolder();


    return null;
  }
}
