package top.chenqs.mychat.common.reactor;

/**
 * Created by chenqs on 2016/7/3.
 */
public class BaseReactor {
	/**
	 * 对消息进行响应
	 * @param spec 能响应的消息的规格
	 * @return 如果响应了消息，则返回true,否则返回false
	 */
	public boolean react(MessageSpec spec) {
		return false;
	}
}
