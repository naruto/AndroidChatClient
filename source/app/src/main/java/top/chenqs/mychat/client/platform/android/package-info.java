/**
 * 该包存放Android平台客户端专用的代码。
 * 所有与Android平台相关的不可移植代码都必须放在该包及其子包下。
 * @see top.chenqs.mychat.client.platform.android.displayer
 * @see top.chenqs.mychat.client.platform.android.ui
 * @see top.chenqs.mychat.client.platform.android.net.socket
 * @version 1.0
 */
package top.chenqs.mychat.client.platform.android;