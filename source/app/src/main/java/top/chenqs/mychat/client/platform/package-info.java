/**
 * 该包是客户端专用的平台相关代码包。
 * 目前只包括Android平台客户端相关的包{@link top.chenqs.mychat.client.platform.android}，
 * 如果需要跨平台，则其他平台的客户端相关的包可以作为本包的子包。
 * 跨平台是指跨Android平台和Java平台
 * @version 1.0
 */
package top.chenqs.mychat.client.platform;