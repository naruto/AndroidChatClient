package top.chenqs.mychat.common.reactor;

/**
 * Created by chenqs on 2016/7/3.
 */
public interface SettingMessageReactor {
	void onSettingHostOk(String new_host);
	void onSettingHostError(String reason);
	void onSettingPortOk(String new_port);
	void onSettingPortError(String reason);
}
