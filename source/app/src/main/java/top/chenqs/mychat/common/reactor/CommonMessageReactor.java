package top.chenqs.mychat.common.reactor;

/**
 * Created by chenqs on 2016/7/3.
 * 消息响应接口
 */
public interface CommonMessageReactor {
	void onError(String reason);
	void onDebug(String reason);
}
