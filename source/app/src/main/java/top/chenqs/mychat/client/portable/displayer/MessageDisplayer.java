package top.chenqs.mychat.client.portable.displayer;

/**
 * Created by chenqs on 2016/6/26.
 */
public interface MessageDisplayer {
        public void showMessage(int type, String content);
}
